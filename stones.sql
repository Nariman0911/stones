/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50641
Source Host           : localhost:3306
Source Database       : stones

Target Server Type    : MYSQL
Target Server Version : 50641
File Encoding         : 65001

Date: 2020-11-30 19:37:08
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `yupe_blog_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_blog`;
CREATE TABLE `yupe_blog_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `icon` varchar(250) NOT NULL DEFAULT '',
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `member_status` int(11) NOT NULL DEFAULT '1',
  `post_status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_blog_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_blog_blog_create_user` (`create_user_id`),
  KEY `ix_yupe_blog_blog_update_user` (`update_user_id`),
  KEY `ix_yupe_blog_blog_status` (`status`),
  KEY `ix_yupe_blog_blog_type` (`type`),
  KEY `ix_yupe_blog_blog_create_date` (`create_time`),
  KEY `ix_yupe_blog_blog_update_date` (`update_time`),
  KEY `ix_yupe_blog_blog_lang` (`lang`),
  KEY `ix_yupe_blog_blog_slug` (`slug`),
  KEY `ix_yupe_blog_blog_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_blog_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_blog_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post`;
CREATE TABLE `yupe_blog_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_id` int(11) NOT NULL,
  `create_user_id` int(11) NOT NULL,
  `update_user_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `publish_time` int(11) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `title` varchar(250) NOT NULL,
  `quote` text,
  `content` text NOT NULL,
  `link` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '0',
  `comment_status` int(11) NOT NULL DEFAULT '1',
  `create_user_ip` varchar(20) NOT NULL,
  `access_type` int(11) NOT NULL DEFAULT '1',
  `meta_keywords` varchar(250) NOT NULL DEFAULT '',
  `meta_description` varchar(250) NOT NULL DEFAULT '',
  `image` varchar(300) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_post_lang_slug` (`slug`,`lang`),
  KEY `ix_yupe_blog_post_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_post_create_user_id` (`create_user_id`),
  KEY `ix_yupe_blog_post_update_user_id` (`update_user_id`),
  KEY `ix_yupe_blog_post_status` (`status`),
  KEY `ix_yupe_blog_post_access_type` (`access_type`),
  KEY `ix_yupe_blog_post_comment_status` (`comment_status`),
  KEY `ix_yupe_blog_post_lang` (`lang`),
  KEY `ix_yupe_blog_post_slug` (`slug`),
  KEY `ix_yupe_blog_post_publish_date` (`publish_time`),
  KEY `ix_yupe_blog_post_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_blog_post_blog` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_create_user` FOREIGN KEY (`create_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_update_user` FOREIGN KEY (`update_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_post_to_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_post_to_tag`;
CREATE TABLE `yupe_blog_post_to_tag` (
  `post_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`post_id`,`tag_id`),
  KEY `ix_yupe_blog_post_to_tag_post_id` (`post_id`),
  KEY `ix_yupe_blog_post_to_tag_tag_id` (`tag_id`),
  CONSTRAINT `fk_yupe_blog_post_to_tag_post_id` FOREIGN KEY (`post_id`) REFERENCES `yupe_blog_post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_post_to_tag_tag_id` FOREIGN KEY (`tag_id`) REFERENCES `yupe_blog_tag` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_post_to_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_tag`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_tag`;
CREATE TABLE `yupe_blog_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_tag_tag_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_blog_user_to_blog`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_blog_user_to_blog`;
CREATE TABLE `yupe_blog_user_to_blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `blog_id` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `note` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_blog_user_to_blog_blog_user_to_blog_u_b` (`user_id`,`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_user_id` (`user_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_id` (`blog_id`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_status` (`status`),
  KEY `ix_yupe_blog_user_to_blog_blog_user_to_blog_role` (`role`),
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_blog_id` FOREIGN KEY (`blog_id`) REFERENCES `yupe_blog_blog` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_blog_user_to_blog_blog_user_to_blog_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_blog_user_to_blog
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_category_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_category_category`;
CREATE TABLE `yupe_category_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `lang` char(2) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_category_category_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_category_category_parent_id` (`parent_id`),
  KEY `ix_yupe_category_category_status` (`status`),
  CONSTRAINT `fk_yupe_category_category_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_category_category
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_comment_comment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_comment_comment`;
CREATE TABLE `yupe_comment_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `model` varchar(100) NOT NULL,
  `model_id` int(11) NOT NULL,
  `url` varchar(150) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `ip` varchar(20) DEFAULT NULL,
  `level` int(11) DEFAULT '0',
  `root` int(11) DEFAULT '0',
  `lft` int(11) DEFAULT '0',
  `rgt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_comment_comment_status` (`status`),
  KEY `ix_yupe_comment_comment_model_model_id` (`model`,`model_id`),
  KEY `ix_yupe_comment_comment_model` (`model`),
  KEY `ix_yupe_comment_comment_model_id` (`model_id`),
  KEY `ix_yupe_comment_comment_user_id` (`user_id`),
  KEY `ix_yupe_comment_comment_parent_id` (`parent_id`),
  KEY `ix_yupe_comment_comment_level` (`level`),
  KEY `ix_yupe_comment_comment_root` (`root`),
  KEY `ix_yupe_comment_comment_lft` (`lft`),
  KEY `ix_yupe_comment_comment_rgt` (`rgt`),
  CONSTRAINT `fk_yupe_comment_comment_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_comment_comment` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_comment_comment_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_comment_comment
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_contentblock_content_block`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_contentblock_content_block`;
CREATE TABLE `yupe_contentblock_content_block` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `code` varchar(100) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `content` text NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_contentblock_content_block_code` (`code`),
  KEY `ix_yupe_contentblock_content_block_type` (`type`),
  KEY `ix_yupe_contentblock_content_block_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_contentblock_content_block
-- ----------------------------
INSERT INTO `yupe_contentblock_content_block` VALUES ('1', 'Телефон:', 'telefon', '1', '<a href=\"tel:+78442566054\">8 (8442) 56-60-54</a> <br>\r\n<a href=\"tel:+79610605666\">8 (961) 060-56-66</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('2', 'E-mail:', 'e-mail', '1', '<a href=\"mailto:info@mycona.ru\">info@mycona.ru</a>', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('3', 'Адрес:', 'adres', '1', 'Ясноморская улица, 2, Волгоград, Волгоградская область, 400039', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('4', 'Политика конфиденциальности', 'politika-konfidencialnosti', '1', '#', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('5', 'Скрипты в шапке', 'skripty-v-shapke', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('6', 'Скрипты в футере', 'skripty-v-futere', '1', '', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('7', 'Карта гугл', 'karta-gugl', '1', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d328.25429523356047!2d44.636558598446136!3d48.83848325242378!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x411ab393455b6481%3A0x360c81b7e7aaa938!2z0K_RgdC90L7QvNC-0YDRgdC60LDRjyDRg9C7LiwgMiwg0JLQvtC70LPQvtCz0YDQsNC0LCDQktC-0LvQs9C-0LPRgNCw0LTRgdC60LDRjyDQvtCx0LsuLCA0MDAwMzk!5e0!3m2!1sru!2sru!4v1606413328602!5m2!1sru!2sru', '', null, '1');
INSERT INTO `yupe_contentblock_content_block` VALUES ('8', 'Номер whatsapp', 'nomer-whatsapp', '1', '+420605255514', '<p>В карточке товара</p>', null, '1');

-- ----------------------------
-- Table structure for `yupe_document`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_document`;
CREATE TABLE `yupe_document` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название документа',
  `file_name` varchar(255) DEFAULT NULL COMMENT 'Имя файла',
  `status` int(11) DEFAULT '0' COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `image` varchar(255) DEFAULT NULL COMMENT 'Превью (изображение)',
  `product_id` int(11) DEFAULT NULL COMMENT 'Для Продукта',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_document
-- ----------------------------
INSERT INTO `yupe_document` VALUES ('1', 'Magicrete каталог товаров', '3b63d4216fad6c7a3e2f702271f75ac5.pdf', '1', '1', '6d13930f32b080139dbf959d6f6fbd6d.PNG', null);

-- ----------------------------
-- Table structure for `yupe_gallery_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_gallery`;
CREATE TABLE `yupe_gallery_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `description` text,
  `status` int(11) NOT NULL DEFAULT '1',
  `owner` int(11) DEFAULT NULL,
  `preview_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_gallery_gallery_status` (`status`),
  KEY `ix_yupe_gallery_gallery_owner` (`owner`),
  KEY `fk_yupe_gallery_gallery_gallery_preview_to_image` (`preview_id`),
  KEY `fk_yupe_gallery_gallery_gallery_to_category` (`category_id`),
  KEY `ix_yupe_gallery_gallery_sort` (`sort`),
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_preview_to_image` FOREIGN KEY (`preview_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_gallery_to_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_gallery_owner` FOREIGN KEY (`owner`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_gallery_image_to_gallery`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_gallery_image_to_gallery`;
CREATE TABLE `yupe_gallery_image_to_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `create_time` datetime NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_gallery_image_to_gallery_gallery_to_image` (`image_id`,`gallery_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_image` (`image_id`),
  KEY `ix_yupe_gallery_image_to_gallery_gallery_to_image_gallery` (`gallery_id`),
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_gallery` FOREIGN KEY (`gallery_id`) REFERENCES `yupe_gallery_gallery` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_gallery_image_to_gallery_gallery_to_image_image` FOREIGN KEY (`image_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_gallery_image_to_gallery
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_image_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_image_image`;
CREATE TABLE `yupe_image_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `description` text,
  `file` varchar(250) NOT NULL,
  `create_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `alt` varchar(250) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_image_image_status` (`status`),
  KEY `ix_yupe_image_image_user` (`user_id`),
  KEY `ix_yupe_image_image_type` (`type`),
  KEY `ix_yupe_image_image_category_id` (`category_id`),
  KEY `fk_yupe_image_image_parent_id` (`parent_id`),
  CONSTRAINT `fk_yupe_image_image_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_parent_id` FOREIGN KEY (`parent_id`) REFERENCES `yupe_image_image` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_image_image_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_image_image
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_mail_mail_event`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_event`;
CREATE TABLE `yupe_mail_mail_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_event_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_event
-- ----------------------------
INSERT INTO `yupe_mail_mail_event` VALUES ('1', 'ostavit-zayavku', 'Оставить заявку', 'форма на странице контакты');
INSERT INTO `yupe_mail_mail_event` VALUES ('2', 'ostavit-zapros', 'Оставить запрос', 'форма в карточке товара');

-- ----------------------------
-- Table structure for `yupe_mail_mail_template`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_mail_mail_template`;
CREATE TABLE `yupe_mail_mail_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(150) NOT NULL,
  `event_id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `description` text,
  `from` varchar(150) NOT NULL,
  `to` varchar(150) NOT NULL,
  `theme` text NOT NULL,
  `body` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_mail_mail_template_code` (`code`),
  KEY `ix_yupe_mail_mail_template_status` (`status`),
  KEY `ix_yupe_mail_mail_template_event_id` (`event_id`),
  CONSTRAINT `fk_yupe_mail_mail_template_event_id` FOREIGN KEY (`event_id`) REFERENCES `yupe_mail_mail_event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_mail_mail_template
-- ----------------------------
INSERT INTO `yupe_mail_mail_template` VALUES ('1', 'ostavit-zayavku', '1', 'Оставить заявку', '', 'kesha56rus@yandex.ru', 'kesha56rus@yandex.ru', 'Заявка theme с сайта CONA', '<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Имя:</strong></td>\r\n<td>name</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Телефон:</strong></td>\r\n<td>phone</td>\r\n</tr>\r\n<tr>\r\n<td><strong>E-mail:</strong></td>\r\n<td>email</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Сообщение:</strong></td>\r\n<td>comment</td>\r\n</tr>\r\n</tbody>\r\n</table>', '1');
INSERT INTO `yupe_mail_mail_template` VALUES ('2', 'ostavit-zapros', '2', 'Оставить запрос', '', 'kesha56rus@yandex.ru', 'kesha56rus@yandex.ru', 'Заявка theme с сайта CONA', '<table>\r\n<tbody>\r\n<tr>\r\n<td><strong>Имя:</strong></td>\r\n<td>name</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Телефон:</strong></td>\r\n<td>phone</td>\r\n</tr>\r\n<tr>\r\n<td><strong>E-mail:</strong></td>\r\n<td>email</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Сообщение:</strong></td>\r\n<td>comment</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Товар:</strong></td>\r\n<td>link</td>\r\n</tr>\r\n</tbody>\r\n</table>', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu`;
CREATE TABLE `yupe_menu_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_menu_menu_code` (`code`),
  KEY `ix_yupe_menu_menu_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu
-- ----------------------------
INSERT INTO `yupe_menu_menu` VALUES ('1', 'Верхнее меню', 'top-menu', 'Основное меню сайта, расположенное сверху в блоке mainmenu.', '1');

-- ----------------------------
-- Table structure for `yupe_menu_menu_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_menu_menu_item`;
CREATE TABLE `yupe_menu_menu_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `regular_link` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(150) NOT NULL,
  `href` varchar(150) NOT NULL,
  `class` varchar(150) DEFAULT NULL,
  `title_attr` varchar(150) DEFAULT NULL,
  `before_link` varchar(150) DEFAULT NULL,
  `after_link` varchar(150) DEFAULT NULL,
  `target` varchar(150) DEFAULT NULL,
  `rel` varchar(150) DEFAULT NULL,
  `condition_name` varchar(150) DEFAULT '0',
  `condition_denial` int(11) DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '1',
  `status` int(11) NOT NULL DEFAULT '1',
  `entity_module_name` varchar(40) DEFAULT NULL,
  `entity_name` varchar(40) DEFAULT NULL,
  `entity_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_menu_menu_item_menu_id` (`menu_id`),
  KEY `ix_yupe_menu_menu_item_sort` (`sort`),
  KEY `ix_yupe_menu_menu_item_status` (`status`),
  CONSTRAINT `fk_yupe_menu_menu_item_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `yupe_menu_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_menu_menu_item
-- ----------------------------
INSERT INTO `yupe_menu_menu_item` VALUES ('12', '0', '1', '1', 'Преимущества', '/advantages', '', '', '', '', '', '', '', '0', '2', '1', '', '', null);
INSERT INTO `yupe_menu_menu_item` VALUES ('13', '0', '1', '1', 'Сотрудничество', '/sotrudnichestvo', null, null, null, null, null, null, '0', '0', '3', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('14', '0', '1', '1', 'Доставка', '/dostavka', null, null, null, null, null, null, '0', '0', '5', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('16', '0', '1', '1', 'Контакты', '/kontakty', null, null, null, null, null, null, '0', '0', '6', '1', null, null, null);
INSERT INTO `yupe_menu_menu_item` VALUES ('18', '0', '1', '1', 'Главная', '/', '', '', '', '', '', '', '', '0', '1', '1', '', '', null);

-- ----------------------------
-- Table structure for `yupe_migrations`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_migrations`;
CREATE TABLE `yupe_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) NOT NULL,
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_migrations_module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_migrations
-- ----------------------------
INSERT INTO `yupe_migrations` VALUES ('1', 'user', 'm000000_000000_user_base', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('2', 'user', 'm131019_212911_user_tokens', '1545824978');
INSERT INTO `yupe_migrations` VALUES ('3', 'user', 'm131025_152911_clean_user_table', '1545824979');
INSERT INTO `yupe_migrations` VALUES ('4', 'user', 'm131026_002234_prepare_hash_user_password', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('5', 'user', 'm131106_111552_user_restore_fields', '1545824980');
INSERT INTO `yupe_migrations` VALUES ('6', 'user', 'm131121_190850_modify_tokes_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('7', 'user', 'm140812_100348_add_expire_to_token_table', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('8', 'user', 'm150416_113652_rename_fields', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('9', 'user', 'm151006_000000_user_add_phone', '1545824981');
INSERT INTO `yupe_migrations` VALUES ('10', 'yupe', 'm000000_000000_yupe_base', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('11', 'yupe', 'm130527_154455_yupe_change_unique_index', '1545824982');
INSERT INTO `yupe_migrations` VALUES ('12', 'yupe', 'm150416_125517_rename_fields', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('13', 'yupe', 'm160204_195213_change_settings_type', '1545824983');
INSERT INTO `yupe_migrations` VALUES ('14', 'category', 'm000000_000000_category_base', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('15', 'category', 'm150415_150436_rename_fields', '1545824984');
INSERT INTO `yupe_migrations` VALUES ('16', 'image', 'm000000_000000_image_base', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('17', 'image', 'm150226_121100_image_order', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('18', 'image', 'm150416_080008_rename_fields', '1545824986');
INSERT INTO `yupe_migrations` VALUES ('19', 'page', 'm000000_000000_page_base', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('20', 'page', 'm130115_155600_columns_rename', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('21', 'page', 'm140115_083618_add_layout', '1545824988');
INSERT INTO `yupe_migrations` VALUES ('22', 'page', 'm140620_072543_add_view', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('23', 'page', 'm150312_151049_change_body_type', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('24', 'page', 'm150416_101038_rename_fields', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('25', 'page', 'm180224_105407_meta_title_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('26', 'page', 'm180421_143324_update_page_meta_column', '1545824989');
INSERT INTO `yupe_migrations` VALUES ('27', 'mail', 'm000000_000000_mail_base', '1545824991');
INSERT INTO `yupe_migrations` VALUES ('28', 'comment', 'm000000_000000_comment_base', '1545824992');
INSERT INTO `yupe_migrations` VALUES ('29', 'comment', 'm130704_095200_comment_nestedsets', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('30', 'comment', 'm150415_151804_rename_fields', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('31', 'notify', 'm141031_091039_add_notify_table', '1545824994');
INSERT INTO `yupe_migrations` VALUES ('32', 'blog', 'm000000_000000_blog_base', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('33', 'blog', 'm130503_091124_BlogPostImage', '1545825001');
INSERT INTO `yupe_migrations` VALUES ('34', 'blog', 'm130529_151602_add_post_category', '1545825002');
INSERT INTO `yupe_migrations` VALUES ('35', 'blog', 'm140226_052326_add_community_fields', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('36', 'blog', 'm140714_110238_blog_post_quote_type', '1545825003');
INSERT INTO `yupe_migrations` VALUES ('37', 'blog', 'm150406_094809_blog_post_quote_type', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('38', 'blog', 'm150414_180119_rename_date_fields', '1545825004');
INSERT INTO `yupe_migrations` VALUES ('39', 'blog', 'm160518_175903_alter_blog_foreign_keys', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('40', 'blog', 'm180421_143937_update_blog_meta_column', '1545825005');
INSERT INTO `yupe_migrations` VALUES ('41', 'blog', 'm180421_143938_add_post_meta_title_column', '1545825006');
INSERT INTO `yupe_migrations` VALUES ('42', 'contentblock', 'm000000_000000_contentblock_base', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('43', 'contentblock', 'm140715_130737_add_category_id', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('44', 'contentblock', 'm150127_130425_add_status_column', '1545825007');
INSERT INTO `yupe_migrations` VALUES ('45', 'menu', 'm000000_000000_menu_base', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('46', 'menu', 'm121220_001126_menu_test_data', '1545825009');
INSERT INTO `yupe_migrations` VALUES ('47', 'menu', 'm160914_134555_fix_menu_item_default_values', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('48', 'menu', 'm181214_110527_menu_item_add_entity_fields', '1545825010');
INSERT INTO `yupe_migrations` VALUES ('49', 'gallery', 'm000000_000000_gallery_base', '1545825012');
INSERT INTO `yupe_migrations` VALUES ('50', 'gallery', 'm130427_120500_gallery_creation_user', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('51', 'gallery', 'm150416_074146_rename_fields', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('52', 'gallery', 'm160514_131314_add_preview_to_gallery', '1545825013');
INSERT INTO `yupe_migrations` VALUES ('53', 'gallery', 'm160515_123559_add_category_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('54', 'gallery', 'm160515_151348_add_position_to_gallery_image', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('55', 'gallery', 'm181224_072816_add_sort_to_gallery', '1545825014');
INSERT INTO `yupe_migrations` VALUES ('56', 'news', 'm000000_000000_news_base', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('57', 'news', 'm150416_081251_rename_fields', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('58', 'news', 'm180224_105353_meta_title_column', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('59', 'news', 'm180421_142416_update_news_meta_column', '1545825016');
INSERT INTO `yupe_migrations` VALUES ('60', 'page', 'm180421_143325_add_column_image', '1547437100');
INSERT INTO `yupe_migrations` VALUES ('61', 'rbac', 'm140115_131455_auth_item', '1547444421');
INSERT INTO `yupe_migrations` VALUES ('62', 'rbac', 'm140115_132045_auth_item_child', '1547444422');
INSERT INTO `yupe_migrations` VALUES ('63', 'rbac', 'm140115_132319_auth_item_assign', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('64', 'rbac', 'm140702_230000_initial_role_data', '1547444423');
INSERT INTO `yupe_migrations` VALUES ('65', 'page', 'm180421_143326_add_column_body_Short', '1574077121');
INSERT INTO `yupe_migrations` VALUES ('66', 'page', 'm180421_143328_add_page_image_tbl', '1574077121');
INSERT INTO `yupe_migrations` VALUES ('67', 'page', 'm180421_143329_add_page_title_page_h1', '1574077122');
INSERT INTO `yupe_migrations` VALUES ('68', 'page', 'm180421_143330_directions_add_column_data', '1590064486');
INSERT INTO `yupe_migrations` VALUES ('70', 'slider', 'm000000_000000_slider_base', '1606400691');
INSERT INTO `yupe_migrations` VALUES ('71', 'slider', 'm000001_000001_slider_add_column', '1606400692');
INSERT INTO `yupe_migrations` VALUES ('72', 'slider', 'm000001_000002_slider_add_column_category', '1606400914');
INSERT INTO `yupe_migrations` VALUES ('73', 'slider', 'm000001_000003_slider_add_column_category_id', '1606401114');
INSERT INTO `yupe_migrations` VALUES ('74', 'store', 'm140812_160000_store_attribute_group_base', '1606401223');
INSERT INTO `yupe_migrations` VALUES ('75', 'store', 'm140812_170000_store_attribute_base', '1606401224');
INSERT INTO `yupe_migrations` VALUES ('76', 'store', 'm140812_180000_store_attribute_option_base', '1606401226');
INSERT INTO `yupe_migrations` VALUES ('77', 'store', 'm140813_200000_store_category_base', '1606401227');
INSERT INTO `yupe_migrations` VALUES ('78', 'store', 'm140813_210000_store_type_base', '1606401227');
INSERT INTO `yupe_migrations` VALUES ('79', 'store', 'm140813_220000_store_type_attribute_base', '1606401228');
INSERT INTO `yupe_migrations` VALUES ('80', 'store', 'm140813_230000_store_producer_base', '1606401229');
INSERT INTO `yupe_migrations` VALUES ('81', 'store', 'm140814_000000_store_product_base', '1606401231');
INSERT INTO `yupe_migrations` VALUES ('82', 'store', 'm140814_000010_store_product_category_base', '1606401233');
INSERT INTO `yupe_migrations` VALUES ('83', 'store', 'm140814_000013_store_product_attribute_eav_base', '1606401234');
INSERT INTO `yupe_migrations` VALUES ('84', 'store', 'm140814_000018_store_product_image_base', '1606401235');
INSERT INTO `yupe_migrations` VALUES ('85', 'store', 'm140814_000020_store_product_variant_base', '1606401236');
INSERT INTO `yupe_migrations` VALUES ('86', 'store', 'm141014_210000_store_product_category_column', '1606401238');
INSERT INTO `yupe_migrations` VALUES ('87', 'store', 'm141015_170000_store_product_image_column', '1606401239');
INSERT INTO `yupe_migrations` VALUES ('88', 'store', 'm141218_091834_default_null', '1606401239');
INSERT INTO `yupe_migrations` VALUES ('89', 'store', 'm150210_063409_add_store_menu_item', '1606401239');
INSERT INTO `yupe_migrations` VALUES ('90', 'store', 'm150210_105811_add_price_column', '1606401240');
INSERT INTO `yupe_migrations` VALUES ('91', 'store', 'm150210_131238_order_category', '1606401240');
INSERT INTO `yupe_migrations` VALUES ('92', 'store', 'm150211_105453_add_position_for_product_variant', '1606401241');
INSERT INTO `yupe_migrations` VALUES ('93', 'store', 'm150226_065935_add_product_position', '1606401241');
INSERT INTO `yupe_migrations` VALUES ('94', 'store', 'm150416_112008_rename_fields', '1606401242');
INSERT INTO `yupe_migrations` VALUES ('95', 'store', 'm150417_180000_store_product_link_base', '1606401244');
INSERT INTO `yupe_migrations` VALUES ('96', 'store', 'm150825_184407_change_store_url', '1606401244');
INSERT INTO `yupe_migrations` VALUES ('97', 'store', 'm150907_084604_new_attributes', '1606401245');
INSERT INTO `yupe_migrations` VALUES ('98', 'store', 'm151218_081635_add_external_id_fields', '1606401246');
INSERT INTO `yupe_migrations` VALUES ('99', 'store', 'm151218_082939_add_external_id_ix', '1606401246');
INSERT INTO `yupe_migrations` VALUES ('100', 'store', 'm151218_142113_add_product_index', '1606401246');
INSERT INTO `yupe_migrations` VALUES ('101', 'store', 'm151223_140722_drop_product_type_categories', '1606401247');
INSERT INTO `yupe_migrations` VALUES ('102', 'store', 'm160210_084850_add_h1_and_canonical', '1606401249');
INSERT INTO `yupe_migrations` VALUES ('103', 'store', 'm160210_131541_add_main_image_alt_title', '1606401251');
INSERT INTO `yupe_migrations` VALUES ('104', 'store', 'm160211_180200_add_additional_images_alt_title', '1606401251');
INSERT INTO `yupe_migrations` VALUES ('105', 'store', 'm160215_110749_add_image_groups_table', '1606401252');
INSERT INTO `yupe_migrations` VALUES ('106', 'store', 'm160227_114934_rename_producer_order_column', '1606401252');
INSERT INTO `yupe_migrations` VALUES ('107', 'store', 'm160309_091039_add_attributes_sort_and_search_fields', '1606401253');
INSERT INTO `yupe_migrations` VALUES ('108', 'store', 'm160413_184551_add_type_attr_fk', '1606401253');
INSERT INTO `yupe_migrations` VALUES ('109', 'store', 'm160602_091243_add_position_product_index', '1606401253');
INSERT INTO `yupe_migrations` VALUES ('110', 'store', 'm160602_091909_add_producer_sort_index', '1606401253');
INSERT INTO `yupe_migrations` VALUES ('111', 'store', 'm160713_105449_remove_irrelevant_product_status', '1606401253');
INSERT INTO `yupe_migrations` VALUES ('112', 'store', 'm160805_070905_add_attribute_description', '1606401254');
INSERT INTO `yupe_migrations` VALUES ('113', 'store', 'm161015_121915_change_product_external_id_type', '1606401255');
INSERT INTO `yupe_migrations` VALUES ('114', 'store', 'm161122_090922_add_sort_product_position', '1606401255');
INSERT INTO `yupe_migrations` VALUES ('115', 'store', 'm161122_093736_add_store_layouts', '1606401256');
INSERT INTO `yupe_migrations` VALUES ('116', 'store', 'm181218_121815_store_product_variant_quantity_column', '1606401257');
INSERT INTO `yupe_migrations` VALUES ('117', 'store', 'm181218_121816_store_category_name_short_icon', '1606401259');
INSERT INTO `yupe_migrations` VALUES ('118', 'store', 'm181218_121821_store_category_add_table_badge', '1606401260');
INSERT INTO `yupe_migrations` VALUES ('119', 'store', 'm181218_121822_store_product_price_result', '1606401261');
INSERT INTO `yupe_migrations` VALUES ('120', 'video', 'm000000_000000_video_base', '1606487938');
INSERT INTO `yupe_migrations` VALUES ('121', 'video', 'm000000_000001_video_add_column', '1606487939');
INSERT INTO `yupe_migrations` VALUES ('122', 'video', 'm000001_000000_video_category_base', '1606487939');
INSERT INTO `yupe_migrations` VALUES ('123', 'video', 'm000002_000001_video_add_column_vimeo', '1606490204');
INSERT INTO `yupe_migrations` VALUES ('124', 'store', 'm181218_121823_store_category_add_is_menu', '1606497981');
INSERT INTO `yupe_migrations` VALUES ('125', 'user', 'm200715_154204_add_super_user', '1606500272');
INSERT INTO `yupe_migrations` VALUES ('126', 'document', 'm000000_000000_document_base', '1606540400');
INSERT INTO `yupe_migrations` VALUES ('127', 'document', 'm000000_000001_document_add_column_image', '1606540401');
INSERT INTO `yupe_migrations` VALUES ('128', 'document', 'm000000_000002_document_add_column_product', '1606540401');
INSERT INTO `yupe_migrations` VALUES ('129', 'store', 'm181218_121824_store_add_column_product_doc_vid', '1606541049');
INSERT INTO `yupe_migrations` VALUES ('130', 'video', 'm000002_000002_video_add_column_is_footer', '1606546412');

-- ----------------------------
-- Table structure for `yupe_news_news`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_news_news`;
CREATE TABLE `yupe_news_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `date` date NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `short_text` text,
  `full_text` text NOT NULL,
  `image` varchar(300) DEFAULT NULL,
  `link` varchar(300) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `meta_title` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_news_news_alias_lang` (`slug`,`lang`),
  KEY `ix_yupe_news_news_status` (`status`),
  KEY `ix_yupe_news_news_user_id` (`user_id`),
  KEY `ix_yupe_news_news_category_id` (`category_id`),
  KEY `ix_yupe_news_news_date` (`date`),
  CONSTRAINT `fk_yupe_news_news_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_news_news_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_news_news
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_notify_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_notify_settings`;
CREATE TABLE `yupe_notify_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `my_post` tinyint(1) NOT NULL DEFAULT '1',
  `my_comment` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_notify_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_notify_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_notify_settings
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_page_page`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page`;
CREATE TABLE `yupe_page_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `lang` char(2) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `change_user_id` int(11) DEFAULT NULL,
  `title_short` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `body` mediumtext NOT NULL,
  `meta_keywords` varchar(250) NOT NULL,
  `meta_description` varchar(250) NOT NULL,
  `status` int(11) NOT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '0',
  `layout` varchar(250) DEFAULT NULL,
  `view` varchar(250) DEFAULT NULL,
  `meta_title` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `icon` varchar(250) DEFAULT NULL,
  `body_short` text,
  `name_h1` varchar(255) DEFAULT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_page_page_slug_lang` (`slug`,`lang`),
  KEY `ix_yupe_page_page_status` (`status`),
  KEY `ix_yupe_page_page_is_protected` (`is_protected`),
  KEY `ix_yupe_page_page_user_id` (`user_id`),
  KEY `ix_yupe_page_page_change_user_id` (`change_user_id`),
  KEY `ix_yupe_page_page_menu_order` (`order`),
  KEY `ix_yupe_page_page_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_page_page_category_id` FOREIGN KEY (`category_id`) REFERENCES `yupe_category_category` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_change_user_id` FOREIGN KEY (`change_user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  CONSTRAINT `fk_yupe_page_page_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page
-- ----------------------------
INSERT INTO `yupe_page_page` VALUES ('1', null, 'ru', null, '2019-01-14 09:39:32', '2020-11-26 22:33:04', '1', '1', 'Главная страница', 'Главная страница', 'glavnaya-stranica', '<h1>О НАС</h1>\r\n<p>Компания CONA<sup>&reg;</sup>&nbsp;является одним из ведущих европейских производителей форм для изготовления искусственного камня, заборов и тротуаров. В линейке нашей продукции вы найдете&nbsp;формы для облицовочной плитки с&nbsp;имитацией камня,&nbsp;кирпичной кладки или дерева,&nbsp;формы для производства тротуарной плитки,&nbsp;а также&nbsp;двухсторонних и односторонних&nbsp;бетоновых заборов с имитацией натурального камня, кирпича, дерева и других материалов.</p>\r\n<p>Нашим основным преимуществом является то, что мы&nbsp;имеем собственное бетонное производство готовых изделий применяя изготовленные нами формы. Именно это позволяет нам гарантировать самое высокое качество продукции и длительный срок службы форм.</p>\r\n<p>За&nbsp;20 лет, что мы имеем собственный производственный цех готовой продукции, мы смогли найти оптимальный материал&nbsp;для&nbsp;изготовления форм,&nbsp;также&nbsp;выявить потенциально слабые стороны наших форм и исправить их. Наша годовая производственная мощность в 100 000 квадратных метров готовой продукции делает нас одними из крупнейших производителей искусственного камня в Европе.</p>', '', '', '1', '0', '1', '', '', '', null, null, '<h2>ПОЧЕМУ ПОКУПАТЬ У НАС:</h2>\r\n<ul>\r\n<li>Продукция от надёжного европейского производителя с солидной историей</li>\r\n<li>Многоуровневая система контроля и применение высококлассного оборудования и материалов для изготовления форм</li>\r\n<li>Оперативная техническая поддержка как при создании, так и в процессе ведения производства</li>\r\n<li>Многолетний опыт в области производства искусственного камня, тротуаров и заборов</li>\r\n</ul>\r\n<h2>ПОЧЕМУ ВЫБРАТЬ ПРОДУКЦИЮ CONA<sup class=\"font_12\">&reg;</sup>:</h2>\r\n<ul>\r\n<li>Высочайшее качество готовых изделий</li>\r\n<li>Длительный срок службы форм</li>\r\n<li>Быстрый возврат инвестиций, благодаря высокому качеству выпускаемой продукции</li>\r\n<li>Полная технологическая документация для производства в соответствии с вашими требованиями</li>\r\n</ul>', '', 'a:1:{i:1;a:8:{s:4:\"name\";s:18:\"Инстаграм\";s:4:\"code\";s:9:\"instagram\";s:8:\"template\";s:10:\"CodeMirror\";s:5:\"value\";s:0:\"\";s:7:\"butName\";s:0:\"\";s:7:\"butLink\";s:1:\"#\";s:5:\"image\";s:24:\"f4b7260bac30a0b7034d.jpg\";s:7:\"gallery\";a:0:{}}}');
INSERT INTO `yupe_page_page` VALUES ('2', null, 'ru', null, '2019-01-14 09:57:27', '2020-11-27 22:12:10', '1', '1', 'Преимущества', 'Преимущества', 'advantages', '<p>Большинством производителей пресс-формы изготавливаются ручным смешиванием эластомерных компонентов. Такой способ изготовления подходит для силиконовых форм. Однако полиуретановые формы, которые в основном присутствуют на рынке, имеют достаточно низкое качество при ручном методе производства. Мы же производим полиуретановые формы, используя сложное современное оборудование с применением высококлассного полиуретана. Это позволяет нам гарантировать высокое качество конечного продукта - формы, котоые не рвуться и имеют минимальный коэффициэнт усадки и растяжения.</p>\r\n<p>Давайте рассмотрим &ldquo;химию&rdquo; полиуретана. БОЛЬШИНСТВО полиуретановых сисем состоят из двух компонентов - полиола и изоцианата. Идеальные свойства готового продукта (формы) могут быть достигнуты только тогда, когда каждая молекула полиоля объединяется с определенным числом молекул изоцианата. Таким образом, материал будет обладать необходимыми свойствами и очень долгим сроком службы.</p>\r\n<p>Вот лишь некоторые из преимуществ полиуретановых пресс-форм:</p>\r\n<ul>\r\n<li><em>Идеальные пропорции при смешивании материала</em><br />Два (а в некоторых случаях три) компонента смешиваются в необходимой пропорции, что позволяет достигнуть полиуретан высокого качества</li>\r\n<li><em>Качественное смешивание всех компонентов</em><br />Это устраняет возможные дефекты в получаемых формах. Эти дефекты не всегда можно обнаружить при первичном контроле качества, а только с течением времени.</li>\r\n<li><em>Контроль температуры</em><br />Даже отлитые при комнатной температуре полиуретановые системы требуют постоянной поддержки особой температуры при смешивании / разливке. Только так можно достичь максимально возможной вязкости материала.</li>\r\n<li><em>Контроль влажности</em><br />При отливке полиуретана необходимо до минимума снизить влажность окружающей среды. Литейный автомат делает это путем вакуумирования полиуретановой смеси. Так решаются сразу две проблемы - из смеси удаляется, как воздух, так и влага. Когда же полиуретан смешивается вручную, происходит прямо противоположное - воздух, а с ним и влага, впитываются в структуру, что заметно снижает качество получаемых пресс-форм.</li>\r\n</ul>\r\n<p>Вот на какие параметры форм влияют факторы, приведенные выше:</p>\r\n<ul>\r\n<li><strong>Резкое уменьшение срока службы</strong><br />Форма, изготовленная литейным автоматом, имеет значительно больший ресурс. Им сложнее нанести механические повреждения, поэтому пресс-формы не теряет нужной геометрии в течение своего использования. При использовании же &ldquo;дешевых&rdquo; форм, которые были изготовлены вручную, вам вскоре придется заменять поврежденные.</li>\r\n<li><strong>Скрытые дефекты</strong><br />Это могут быть &ldquo;недомесы&rdquo; или неточное соблюдение пропорций компонентов при смешивании. Это также существенно снижает качество и, в свою очередь, требует частой замены таких пресс-форм.</li>\r\n<li><strong>Адгезия / истирание / устойчивость к агрессивной среде</strong><br />Полиуретаны высшего качества (разработанные специально для бетонной отливки) могут обрабатываться только роботом и не могут смешиваться вручную.</li>\r\n</ul>', '', '', '1', '0', '2', '', '', '', null, null, '', 'Преимущества продукции MAGICRETE', null);
INSERT INTO `yupe_page_page` VALUES ('3', null, 'ru', null, '2019-01-14 09:57:56', '2020-11-27 22:13:40', '1', '1', 'Сотрудничество', 'Сотрудничество', 'sotrudnichestvo', '<p>Наше клиентское портфолио уже достаточно велико, поэтому мы можем найти индивидуальное решение практически для каждого клиента. Не важно, планируете ли вы наладить производство искусственного камня, забрных систем, тротуарной плитки, или у вас уже есть полномасштабное производство и вы решили расширить свой ассортимент - в каждом случае мы в состоянии предложить продукт или услугу, которые вы ищете.</p>\r\n<p>Наш богатый опыт в области производства искусственного камня, заборов и других бетонных изделий позволяет нам точно определить ваши потребности и создать индивидуальное решение, которое будет соответствовать конкретно вашим требованиям.</p>\r\n<p>Чтобы принять правильное решение о типе сотрудничества с нашей компанией, советуем лично посетить нас в Праге. В нашем салоне вы сможете испытать превосходное качество продукции и обсудить возможности сотрудничества.</p>\r\n<p>В зависимости от потребностей наших клиентов и инвестиционных возможностей мы можем предложить один из следующих типов сотрудничества:</p>\r\n<ul>\r\n<li><strong>Поставка только пресс-форм</strong><br />&nbsp;<br />Наша компания поставляет пресс-формы по спецификации заказчика. Мы также предоставляем нашему клиенту основную информацию о рецептах бетонной смеси и оборудовании, необходимых для производства бетона.</li>\r\n</ul>\r\n<ul>\r\n<li><strong>Поставка пресс-форм и бетонных добавок</strong><br /><br />Мы будем поставлять пресс-формы и бетонные добавки по спецификации клиента. Мы также предоставим основную информацию о рецептах конкретных смесей, процентном содержании пигмента для цельной окраски и перечень необходимого оборудования для производства бетона.</li>\r\n</ul>\r\n<ul>\r\n<li><strong>Поставка пресс-форм, добавок к бетону и частичная передача технологии производства MAGICRETE клиенту<br />&nbsp;<br /></strong>При этом варианте мы поставляем клиенту формы, химические добавки для бетона и отдельные декоративные средства. Частичный перенос технологии производства MAGICRETE осуществляется на нашем производственном предприятии в Чехии, недалеко от Праги. Он &nbsp;включает в себя следующее: базовая теория, подготовка правильной бетонной смеси, технологические особенности созревания изделий, создание интегральной / поверхностной декорации, логистика производства в целом.<br /><br />После прохождения обучения, которое занимает 3-4 дня (включая ПРАКТИЧЕСКУЮ часть), вы сможете производить некоторые из продуктов MAGICRETE в том же качестве, что и оригинальные продукты. Основываясь на полученных знаниях, вы сможете создавать новые типы декораций, расширяя ассортимент.</li>\r\n</ul>\r\n<ul>\r\n<li><strong>Поставка пресс-форм, добавок для бетона и ПОЛНАЯ ПЕРЕДАЧА технологии производства MAGICRETE</strong><br /><br />Этот вариант является комплексным решением, которое позволит вам начать полномасштабное производство искусственного камня и других конкретных изделий. В этом случае мы поставляем клиенту формы, добавки для бетона и полный спектр декоративных красителей (интегральная окраска, окраска поверхности и специальные пятна).<br /><br />Учебный курс длится от 7 до 10 дней, проводится на производственном объекте MAGICRETE около Праги, в Чехии. В течении курса ваши сотрудники получат всю информацию относительно организации и ведения производства искусственного камня / заборов / тротуарной плитки, получат практические знания о том, как правильно подготовить бетонную смесь, узнать, как окрасить изделия ( как интегральной, так и поверхностной, с применением специальных красителей). При данном типе сотрудничества вы сможете производить полный ассортимент продукции MAGICRETE и вводить новые типы отделки на основе полученных знаний.</li>\r\n</ul>\r\n<p>Хотим отметить, что все учебные курсы, предоставляемые MAGICRETE, включают как теоретические, так и практические части. Ваш персонал будет проводить лишь небольшую часть времени в классе, а большую непосредственно на производственном объекте, принимая участие в этом процессе и получая практические знания. Мы считаем, что это единственно правильный способ передачи знаний.</p>\r\n<p>Все варианты, указанные выше, являются лишь примерами возможных типов сотрудничества, и в зависимости от ваших конкретных потребностей мы подготовим индивидуальное решение.</p>', '', '', '1', '0', '3', '', '', '', null, null, '', 'Типы сотрудничества', null);
INSERT INTO `yupe_page_page` VALUES ('4', null, 'ru', null, '2019-01-14 09:58:14', '2020-11-27 22:23:24', '1', '1', 'Доставка', 'Доставка', 'dostavka', '<p>Мы осознаем тот факт, что вопрос доставки может быть проблемой для некоторых клиентов, особенно если они раньше не занимались логистикой. Не имея опыта в этой области, можно отпугнуть некоторых потенциальных клиентов, поскольку они опасаются столкнуться с потенциальными препятствиями при транспортировке своих товаров. Именно поэтому MAGICRETE предлагает полный комплекс логистических услуг при доставке вашего заказа.</p>\r\n<p>Имея многолетний опыт в области логистики, мы можем доставить вам заказ практически в любую точку мира. Мы сотрудничаем с известными транспортными компаниями, чтобы обеспечить быструю, надежную и экономичную доставку вашего заказа.</p>\r\n<p>Если вы хотите организовать самостоятельную доставку товаров, мы подготовим необходимую документацию, и вы сможете получить свой заказ прямо со склада.</p>\r\n<p>Доставка гибких форм требует особого внимания к упаковке товаров. Все формы надежно упакованы в деревянные ящики, для того, чтобы гарантировать, что при доставке не будет никаких повреждений. Таким образом, вы можете быть уверены, что ваш заказ будет доставлен в идеальном состоянии, даже если ваш пункт назначения находится за тысячи километров от нашего склада.</p>', '', '', '1', '0', '4', '', '', '', null, null, '', 'Доставка вашего заказа', null);
INSERT INTO `yupe_page_page` VALUES ('6', null, 'ru', null, '2019-01-14 09:58:44', '2020-11-28 18:25:48', '1', '1', 'Контакты', 'Контакты', 'kontakty', '<table style=\"font-weight: 400;\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td style=\"width: 45.7241%;\">\r\n<h3>MAGICRETE S.R.O.</h3>\r\n</td>\r\n<td style=\"width: 54.1619%;\">\r\n<h3><u>ЦЕНТРАЛЬНЫЙ ОФИС:</u></h3>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style=\"width: 45.7241%;\">\r\n<p><strong>Телефон для связи:</strong><br />(<em>Мы говорим по русски!</em>)<br /><strong><br />+420 605 255 514&nbsp;<em>(WhasApp, Viber)</em><br />+420 257 951 521</strong></p>\r\n<p><strong>Номер факса:</strong><br />+420 251 619 289</p>\r\n<p><strong>E-mail:&nbsp;</strong><a href=\"mailto:info@kamen-ploty.cz\"><strong>info@kamen-ploty.cz</strong></a></p>\r\n</td>\r\n<td style=\"width: 54.1619%;\">\r\n<p>Центральный офис и шоурум находятся<br />в 10 минутах езды от Пражского аэропорта:<br /><br /><strong>MAGICRETE s.r.o.</strong><br /><strong>Chrast\'any 140,<br />25219 Rudna u Prahy<br />Czech republic<br /></strong><strong><br /><br /><a href=\"https://goo.gl/maps/UauZKLevmVq\">Показать на карте (Google maps)</a></strong></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p><a href=\"#\" target=\"_blank\" rel=\"noopener\"><img src=\"/uploads/image/9069b2a13692d8834285aca4e19f5f6f.jpg\" width=\"246\" height=\"75\" /></a><br /><br /><br /></p>\r\n<table style=\"border-collapse: collapse; width: 100%;\" width=\"100%\" cellspacing=\"1\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td>\r\n<p><em><u>CONTACT PERSONNEL:</u></em></p>\r\n</td>\r\n<td>\r\n<p>&nbsp;</p>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><img src=\"/uploads/image/053a40cacb82636b4149b91090aff73d.jpg\" alt=\"\" width=\"90\" height=\"120\" /></td>\r\n<td>\r\n<p>&nbsp; <strong>Mark Dvorkin</strong><br />&nbsp;&nbsp;<em>C</em><em>OMMERCIAL DIRECTOR<br /></em><em><br />&nbsp; Tel.: +420605255512</em><em><br />&nbsp;&nbsp;&nbsp;</em><em><a href=\"mailto:mark.d@magicrete.cz?subject=Enquiry\"><strong>mark.d@magicrete.cz</strong></a></em><em><u><br /></u></em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td>\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td><img src=\"/uploads/image/f53020f5d5f91a61f9cd4e5016f7397c.jpg\" alt=\"\" width=\"90\" height=\"114\" /></td>\r\n<td>\r\n<p><strong>&nbsp; </strong><strong>Michal Miskay</strong><br /><em>&nbsp;&nbsp;EXPERT SALESMAN<br /><br />&nbsp; Tel.: +420730149584</em><em><br />&nbsp;&nbsp;&nbsp;<a href=\"mailto:michal.m@magicrete.cz?subject=Enquiry\"><strong>michal.m@magicrete.cz</strong></a><br /></em></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">&nbsp;</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>&nbsp;</p>\r\n<p><img src=\"/uploads/image/9412eae55d64035b87b2275eeed78058.jpg\" /></p>\r\n<p>&nbsp;</p>\r\n<h3 class=\"blocks_title\">ВЫ МОЖЕТЕ СВЯЗАТЬСЯ С НАМИ ЧЕРЕЗ ФОРМУ ОБРАТНОЙ СВЯЗИ:</h3>', '', '', '1', '0', '6', '', 'view-contact', '', null, null, '', '', null);
INSERT INTO `yupe_page_page` VALUES ('7', null, 'ru', null, '2020-11-27 23:05:47', '2020-11-27 23:15:23', '1', '1', 'Каталог', 'Каталог', 'catalog', '', '', '', '1', '0', '7', '', 'view-catalog', '', null, null, '', '', null);
INSERT INTO `yupe_page_page` VALUES ('8', null, 'ru', null, '2020-11-28 19:11:50', '2020-11-28 19:11:50', '1', '1', 'Видео', 'Видео', 'video', '<p>[[w:Video]]</p>', '', '', '1', '0', '8', '', '', '', null, null, '', '', null);

-- ----------------------------
-- Table structure for `yupe_page_page_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_page_page_image`;
CREATE TABLE `yupe_page_page_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_page_page_image
-- ----------------------------
INSERT INTO `yupe_page_page_image` VALUES ('1', '4', '115a0a9c205292eb90ef72487ad17a9b.jpg', '', '', '4');
INSERT INTO `yupe_page_page_image` VALUES ('2', '4', 'ae1304f18f5a509f2cc86e08c69b0a42.jpg', '', '', '3');
INSERT INTO `yupe_page_page_image` VALUES ('3', '4', 'f1e47acecd47028892a8e8651ae30a75.jpg', '', '', '1');
INSERT INTO `yupe_page_page_image` VALUES ('4', '4', '8bae629475c4688a671f6e421ec7d39c.jpg', '', '', '2');
INSERT INTO `yupe_page_page_image` VALUES ('5', '4', '2a7563e973e1b11a5db99e293426b9d9.jpg', '', '', '5');

-- ----------------------------
-- Table structure for `yupe_slider`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_slider`;
CREATE TABLE `yupe_slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `name_short` varchar(255) DEFAULT NULL COMMENT 'Короткое Название',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение',
  `description` text COMMENT 'Описание',
  `description_short` text COMMENT 'Краткое описание',
  `button_name` varchar(255) DEFAULT NULL COMMENT 'Название кнопки',
  `button_link` varchar(255) DEFAULT NULL COMMENT 'url для кнопки',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `image_xs` varchar(250) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  `storecategory_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_slider
-- ----------------------------
INSERT INTO `yupe_slider` VALUES ('1', 'слайд1', null, 'b17edb6874e8340e4d4ad921520bc575.jpg', null, null, null, null, '1', '1', null, '1', null);
INSERT INTO `yupe_slider` VALUES ('2', 'слайд2', null, '04a10c89747564c9e6b923987eccee8c.jpg', null, null, null, null, '1', '2', null, '1', null);
INSERT INTO `yupe_slider` VALUES ('3', 'Преимущества слайд1', null, 'cf27901147b6e3b209d75f00608e92cc.jpg', null, null, null, null, '1', '3', null, '2', null);
INSERT INTO `yupe_slider` VALUES ('4', 'Сотрудничество слайд1', null, '6a50161a8422fee082f3765b3cd37493.jpg', null, null, null, null, '1', '4', null, '3', null);
INSERT INTO `yupe_slider` VALUES ('5', 'Доставка слайд1', null, 'dc80573e75c07c7ae5b61310e0482d9b.jpg', null, null, null, null, '1', '5', null, '4', null);
INSERT INTO `yupe_slider` VALUES ('6', 'Формы для камня слайд1', null, 'd4be31c01ee497b48d06d779bd35127b.jpg', null, null, null, null, '1', '6', null, null, '1');
INSERT INTO `yupe_slider` VALUES ('7', 'Контакты слайд1', null, '16c5e0b3e5cd2f6843d5a1a1ab90c018.jpg', null, null, null, null, '1', '7', null, '6', null);

-- ----------------------------
-- Table structure for `yupe_store_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_attribute`;
CREATE TABLE `yupe_store_attribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `unit` varchar(30) DEFAULT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(11) NOT NULL DEFAULT '0',
  `is_filter` smallint(6) NOT NULL DEFAULT '1',
  `description` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_attribute_name_group` (`name`,`group_id`),
  KEY `ix_yupe_store_attribute_title` (`title`),
  KEY `fk_yupe_store_attribute_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_attribute_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_attribute_group` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_attribute_group`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_attribute_group`;
CREATE TABLE `yupe_store_attribute_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_attribute_group
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_attribute_option`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_attribute_option`;
CREATE TABLE `yupe_store_attribute_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_attribute_option_attribute_id` (`attribute_id`),
  KEY `ix_yupe_store_attribute_option_position` (`position`),
  CONSTRAINT `fk_yupe_store_attribute_option_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_attribute_option
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_badge`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_badge`;
CREATE TABLE `yupe_store_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `color` varchar(255) DEFAULT NULL COMMENT 'Цвет текста',
  `background` text COMMENT 'Фон Badge',
  `status` int(11) DEFAULT NULL COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_badge
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_category`;
CREATE TABLE `yupe_store_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `slug` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  `name_short` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `name_desc` text,
  `is_menu` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_category_alias` (`slug`),
  KEY `ix_yupe_store_category_parent_id` (`parent_id`),
  KEY `ix_yupe_store_category_status` (`status`),
  KEY `yupe_store_category_external_id_ix` (`external_id`),
  CONSTRAINT `fk_yupe_store_category_parent` FOREIGN KEY (`parent_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_category
-- ----------------------------
INSERT INTO `yupe_store_category` VALUES ('1', null, 'formy-dlya-iskusstvennogo-kamnya', 'Формы для искусственного камня', '368c78d149c3996506e96a08d53bc5de.jpg', '', '<h2 class=\"h1\">ФОРМЫ ДЛЯ ИСКУССТВЕННОГО КАМНЯ</h2>\r\n<p>Искусственный камень изготавливается из бетона или иного состава смеси с использованием эластичных форм и имеет широкий спектр применения для фасадной&nbsp;и внутренней отделки жилых домов,&nbsp;квартир, кафе и ресторанов,&nbsp;офисов и коммерческих зданий. Компания MAGICRETE&nbsp;предлагает&nbsp;широкий выбор форм для производства различных видов облицовочной плитки. Наш ассортимент включает в себя большое количество форм с имитацией натурального камня,&nbsp;дерева&nbsp;и&nbsp;кирпича. Выбирая продукцию MAGICRETE<sup>&reg;</sup>, Вы получаете полиуретановые формы высочайшего качества и долгим сроком службы,&nbsp;а таже полную техническую поддержку при создании и ведении производства.</p>\r\n<h3>ПРЕИМУЩЕСТВА ФОРМ MAGICRETE<sup>&reg;</sup>:</h3>\r\n<ul>\r\n<li><strong>Высококачественный полиуретан, разработанный специально для технологии вибролитья.</strong><br />Полиуретан, с применением которого изготавливаются формы MAGICRETE, имеет высокую устойчивость&nbsp;к механическому воздействию и минимальный коэффицент усадки, что гарантирует длительный срок эксплуатации.</li>\r\n<li><strong>Все наши&nbsp;формы производятся автоматизированным оборудованием для&nbsp;литья полиуретана.</strong><br />Применение литьевого оборудования австрийской компании POLYTEC позволяет добиться высочайшего качества форм и обеспечивает их долгий срок службы.</li>\r\n<li><strong>Все модели разработаны на собственном производстве MAGICRETE<sup>&reg;</sup>.</strong><br />Все модели продукции были разработаны с применением натурального камня, кирпича и дерева. Текстуры бережно моделируются с применением силикона и переносятся в формы, поэтому все детали текстуры&nbsp;будут видны на готовом изделии.</li>\r\n<li><strong>MAGICRETE<sup>&reg;</sup>&nbsp;</strong><strong>является крупым Европейским производителем искусственного камня.</strong><br />Как много компаний, продающих формы,&nbsp;также&nbsp;имеют собственное производство готовой продукции? Совсем мало! MAGICRETE<sup>&reg;</sup>&nbsp;является ведущим европейским производителем искусственного &nbsp;камня,&nbsp;заборных&nbsp;систем&nbsp;и тротуарной плитки. На продажу мы предлагаем те же самые формы, которые применяем на своём произвостве.</li>\r\n<li><strong>Строгий контроль качества</strong><br />Все наши продукты проходят&nbsp;жёсткий контроль качества на всех этапах производства. Это делается для того, чтобы гарантировать поставку изделий высочайшего качества и долгим сроком службы.</li>\r\n</ul>', '', '', '', '1', '1', null, 'Формы для искусственного камня', '', '', '', '', 'Формы для камня', null, '1', '', '1');
INSERT INTO `yupe_store_category` VALUES ('2', null, 'formy-dlya-zaborov', 'Формы для заборов', '817c658a6400506b9f2815dc0c4f6610.jpg', '', '', '', '', '', '1', '2', null, '', '', '', '', '', 'Формы для заборов', null, '1', '', '1');
INSERT INTO `yupe_store_category` VALUES ('3', null, 'formy-dlya-trotuarov', 'Формы для тротуаров', '448578c359ba739e87f5ce525bd7a83f.jpg', '', '', '', '', '', '1', '3', null, '', '', '', '', '', 'Формы для тротуаров', null, '1', '', '1');
INSERT INTO `yupe_store_category` VALUES ('4', null, 'kislotnyy-krasitel', 'Кислотный краситель', 'e95936186c6dc96d5d91a0a59428ecec.jpg', '', '', '', '', '', '1', '4', null, '', '', '', '', '', 'Кислотный краситель', null, '1', '', '1');
INSERT INTO `yupe_store_category` VALUES ('5', null, 'pigmenty', 'Пигменты', 'a009c7661156e3637e3473ec11e6fd1d.jpg', '', '', '', '', '', '1', '5', null, '', '', '', '', '', 'Пигменты', null, '1', '', '1');
INSERT INTO `yupe_store_category` VALUES ('6', null, 'dobavki', 'Добавки', '2116f84deed5ed85c74387ec67f2f8ac.jpg', '', '', '', '', '', '1', '6', null, '', '', '', '', '', 'Добавки', null, '1', '', '1');
INSERT INTO `yupe_store_category` VALUES ('7', '1', 'formy-dlya-kamnya', 'Формы для камня', null, '', '', '', '', '', '1', '7', null, '', '', '', '', '', 'Формы для камня', null, '0', null, '0');
INSERT INTO `yupe_store_category` VALUES ('8', '1', 'formy-dlya-kirpicha', 'Формы для кирпича', null, '', '', '', '', '', '1', '8', null, '', '', '', '', '', 'Формы для кирпича', null, '0', null, '0');

-- ----------------------------
-- Table structure for `yupe_store_producer`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_producer`;
CREATE TABLE `yupe_store_producer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_short` varchar(150) NOT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `image` varchar(250) DEFAULT NULL,
  `short_description` text,
  `description` text,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '0',
  `view` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_producer_slug` (`slug`),
  KEY `ix_yupe_store_producer_sort` (`sort`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_producer
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product`;
CREATE TABLE `yupe_store_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `producer_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sku` varchar(100) DEFAULT NULL,
  `name` varchar(250) NOT NULL,
  `slug` varchar(150) NOT NULL,
  `price` decimal(19,3) NOT NULL DEFAULT '0.000',
  `discount_price` decimal(19,3) DEFAULT NULL,
  `discount` decimal(19,3) DEFAULT NULL,
  `description` text,
  `short_description` text,
  `data` text,
  `is_special` tinyint(1) NOT NULL DEFAULT '0',
  `length` decimal(19,3) DEFAULT NULL,
  `width` decimal(19,3) DEFAULT NULL,
  `height` decimal(19,3) DEFAULT NULL,
  `weight` decimal(19,3) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `in_stock` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `meta_title` varchar(250) DEFAULT NULL,
  `meta_keywords` varchar(250) DEFAULT NULL,
  `meta_description` varchar(250) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `average_price` decimal(19,3) DEFAULT NULL,
  `purchase_price` decimal(19,3) DEFAULT NULL,
  `recommended_price` decimal(19,3) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `external_id` varchar(100) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `meta_canonical` varchar(255) DEFAULT NULL,
  `image_alt` varchar(255) DEFAULT NULL,
  `image_title` varchar(255) DEFAULT NULL,
  `view` varchar(100) DEFAULT NULL,
  `is_home` tinyint(1) NOT NULL DEFAULT '0',
  `price_result` decimal(19,3) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_alias` (`slug`),
  KEY `ix_yupe_store_product_status` (`status`),
  KEY `ix_yupe_store_product_type_id` (`type_id`),
  KEY `ix_yupe_store_product_producer_id` (`producer_id`),
  KEY `ix_yupe_store_product_price` (`price`),
  KEY `ix_yupe_store_product_discount_price` (`discount_price`),
  KEY `ix_yupe_store_product_create_time` (`create_time`),
  KEY `ix_yupe_store_product_update_time` (`update_time`),
  KEY `fk_yupe_store_product_category` (`category_id`),
  KEY `yupe_store_product_external_id_ix` (`external_id`),
  KEY `ix_yupe_store_product_sku` (`sku`),
  KEY `ix_yupe_store_product_name` (`name`),
  KEY `ix_yupe_store_product_position` (`position`),
  CONSTRAINT `fk_yupe_store_product_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_producer` FOREIGN KEY (`producer_id`) REFERENCES `yupe_store_producer` (`id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product
-- ----------------------------
INSERT INTO `yupe_store_product` VALUES ('1', null, null, '7', '', 'Серия M1', 'm1-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Кол-во в комплекте</th>\r\n<th>Размер элементов</th>\r\n<th>Общая площадь</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>M1-60020A</td>\r\n<td>1</td>\r\n<td>60 x 20 cm</td>\r\n<td>0,24 m<sup>2</sup></td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>M1-60020B</td>\r\n<td>1</td>\r\n<td>60 x 20 cm</td>\r\n<td>0,24 m<sup>2</sup></td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th> </th>\r\n<th> </th>\r\n<th>0,48 m2</th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/bd33de50a1c87b3b78afa4f4b4f48ff6.jpg\" alt=\"bd33de50a1c87b3b78afa4f4b4f48ff6.jpg\" /></p>', '', 'M1-60020A, M1-60020B', '0', null, null, null, null, null, '1', '1', '2020-11-28 00:17:54', '2020-11-30 19:26:32', '', '', '', 'dca40dd96d462050060018aee61562fc.jpg', null, null, null, '1', null, '', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('2', null, null, '7', '', 'Серия RKS', 'rks-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Кол-во в комплекте</th>\r\n<th>Размер элементов</th>\r\n<th>Общая площадь</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>RKS6060A</td>\r\n<td>1</td>\r\n<td>55 x 12 cm</td>\r\n<td>0,264 m<sup>2</sup></td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>RKS6060B</td>\r\n<td>1</td>\r\n<td>55 x 12 cm</td>\r\n<td>0,264 m<sup>2</sup></td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>RKS6060C</td>\r\n<td>1</td>\r\n<td>55 x 12 cm</td>\r\n<td>0,264 m<sup>2</sup></td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th> </th>\r\n<th> </th>\r\n<th>0,792 m2</th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/f7e3806b6ce1bec1ee20220c98dc637a.jpg\" alt=\"f7e3806b6ce1bec1ee20220c98dc637a.jpg\" /></p>', '', 'RKS6060A, RKS6060B, RKS6060C', '0', null, null, null, null, null, '1', '1', '2020-11-28 11:37:21', '2020-11-30 18:55:43', '', '', '', '266cc305edd77204bea98f457bd1a014.jpg', null, null, null, '2', null, '', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('3', null, null, '7', '', 'Серия TVD', 'tvd-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Размер элементов</th>\r\n<th>Общая площадь</th>\r\n<th>Толщина изделия</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>TVD5050A-E</td>\r\n<td>50 x 20 cm / 50 x 210 cm</td>\r\n<td>0,21 m<sup>2</sup></td>\r\n<td>3,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>TVD5050G</td>\r\n<td>20 x 20 cm / 10 x 210 cm</td>\r\n<td>0,25 m<sup>2</sup></td>\r\n<td>3,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>TVD5050H</td>\r\n<td>50 x 210 cm</td>\r\n<td>0,25 m<sup>2</sup></td>\r\n<td>3,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>TVD5050H</td>\r\n<td>50 x 210 cm</td>\r\n<td>0,25 m<sup>2</sup></td>\r\n<td>3,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>TVD5050A-E</td>\r\n<td>50 x 20 cm / 50 x 210 cm</td>\r\n<td>0,21 m<sup>2</sup></td>\r\n<td>3,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th>1,76 m<sup>2</sup></th>\r\n<th> </th>\r\n<th> </th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/cdcf6eae679cfb1e1b5a79d69b5454a4.jpg\" alt=\"cdcf6eae679cfb1e1b5a79d69b5454a4.jpg\" /></p>\r\n<h3>TVD - ФОРМЫ УГЛОВЫХ ЭЛЕМЕНТОВ</h3>\r\n<p><img src=\"/uploads/image/aadf4ac1122a5800f40d3d38518f99f1.jpg\" alt=\"aadf4ac1122a5800f40d3d38518f99f1.jpg\" /></p>\r\n<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Площадь изделия</th>\r\n<th>Формат изделия</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>TVDC Полиуретановая вкладка</td>\r\n<td>0,5 погонных метров угла</td>\r\n<td>20 x 10 x 10 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>TVDM Форма с металлическим корпусом</td>\r\n<td>0,5 погонных метров угла</td>\r\n<td>20 x 10 x 10 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'TVD5050A, TVD5050B, TVD5050C, TVD5050D, TVD5050E, TVD5050F, TVD5050G, TVD5050H, TVD5050A-E, TVD5050A-E, TVDC Полиуретановая вкладка, TVDM Форма с металлическим корпусом', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:14:33', '2020-11-30 19:28:20', '', '', '', '7d03da07a3e70220406c2980f113ba9b.jpg', null, null, null, '3', null, '', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('4', null, null, '7', '', 'Серия BSL', 'bsl-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Площадь формы</th>\r\n<th>Формат изделия</th>\r\n<th>Кол-во форм в комплекте</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>BSL5045 A-D</td>\r\n<td>0,2 m2</td>\r\n<td>43 x 11,6 cm</td>\r\n<td>4</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>BSL4520 ½</td>\r\n<td>0,1 m2</td>\r\n<td>43 x 11,6 cm</td>\r\n<td>1</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>BSL4520 ¾</td>\r\n<td>0,1 m2</td>\r\n<td>21,5 x 11,6 cm</td>\r\n<td>1</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th>1 m<sup>2</sup></th>\r\n<th> </th>\r\n<th> </th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/eff8c6ad1a4ef7f678ac907a44cbc62f.jpg\" alt=\"eff8c6ad1a4ef7f678ac907a44cbc62f.jpg\" /></p>\r\n<h3>BSL - ФОРМЫ УГЛОВЫХ ЭЛЕМЕНТОВ</h3>\r\n<p><img src=\"/uploads/image/2b123089e44040ddfdf1599cea2e6bf2.jpg\" alt=\"2b123089e44040ddfdf1599cea2e6bf2.jpg\" /></p>\r\n<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Площадь изделий</th>\r\n<th>Формат изделий</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>BSLC Полиуретановая вкладка</td>\r\n<td>0,58 погонных метров угла</td>\r\n<td>21,5 x 11,6 x 10,7 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>BSLC2M Форма с металлическим корпусом</td>\r\n<td>0,58 погонных метров угла</td>\r\n<td>21,5 x 11,6 x 10,7 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'BSL5045A, BSL5045B, BSL5045C, BSL5045D, BSL4520 ½, BSL4520 ¾, BSL5045 A-D, BSLC Полиуретановая вкладка, BSLC2M Форма с металлическим корпусом', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:16:11', '2020-11-30 19:31:23', '', '', '', 'feb8a13a0b4bc12a64a970f61bb0107c.jpg', null, null, null, '4', null, 'СЕРИЯ BSL – КОЛОТЫЙ МРАМОР', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('5', null, null, '7', '', 'Серия KVR', 'kvr-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Общая площадь</th>\r\n<th>Кол-во форм в комплекте</th>\r\n<th>Формат изделия</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>KVR5040A</td>\r\n<td>0,2 m<sup>2</sup></td>\r\n<td>1 штука</td>\r\n<td>40 x 10 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>KVR5040B</td>\r\n<td>0,2 m<sup>2</sup></td>\r\n<td>1 штука</td>\r\n<td>40 x 10 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>KVR5040C</td>\r\n<td>0,2 m<sup>2</sup></td>\r\n<td>1 штука</td>\r\n<td>40 x 10 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th>0,6 m<sup>2</sup></th>\r\n<th> </th>\r\n<th> </th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/ae15f1d6c3ccecc0b287468a7f6e2fa6.jpg\" alt=\"ae15f1d6c3ccecc0b287468a7f6e2fa6.jpg\" /></p>', '', 'KVR5040A, KVR5040B, KVR5040C', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:16:52', '2020-11-30 18:57:44', '', '', '', 'f90ddc44b203443db0edffca21c486c1.jpg', null, null, null, '5', null, 'СЕРИЯ KVR - КВАРЦИТ', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('6', null, null, '7', '', 'Серия PBK', 'pbk-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Площадь поверхности</th>\r\n<th>Размер элементов</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>PBK6050 A-D</td>\r\n<td>0,25 m<sup>2</sup></td>\r\n<td>30,6 x 13,6 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>PBKC6045 A/B</td>\r\n<td>0,54 running meters</td>\r\n<td>30,6 x 13,6 cm, 16,1 x 13,6 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th>1 m<sup>2</sup></th>\r\n<th> </th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/246ef52d08ac2f5e36e019d9cc3c58d9.jpg\" alt=\"246ef52d08ac2f5e36e019d9cc3c58d9.jpg\" /></p>', '', 'PBK6050A, PBK6050B, PBK6050D, PBKC6045B, PBK6050 A-D, PBKC6045 A/B', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:17:33', '2020-11-30 19:33:06', '', '', '', 'da1c22496778851534e9f8450e28ec8f.jpg', null, null, null, '6', null, 'СЕРИЯ PBK - КВАРЦИТ', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('7', null, null, '7', '', 'Серия WDS', 'wds-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th width=\"25%\">Индекс формы</th>\r\n<th width=\"25%\">Общая площадь в 1 фоме</th>\r\n<th width=\"25%\">Толщина изделия</th>\r\n<th style=\"width:12.5%;\" width=\"25%\">Размер элементов</th>\r\n<th style=\"width:12.5%;\">Цена</th>\r\n</tr>\r\n<tr>\r\n<td>WDS575165</td>\r\n<td>0,19 m<sup>2</sup></td>\r\n<td>3 cm</td>\r\n<td>60 x 16,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>WDS601654A/B/C/D</td>\r\n<td>0,2 m<sup>2</sup> (x 4 pcs.)</td>\r\n<td>3 cm</td>\r\n<td>60 x 16,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>WDS30165</td>\r\n<td>0,1 m<sup>2</sup></td>\r\n<td>3 cm</td>\r\n<td>30 x 16,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>WDS27165</td>\r\n<td>0,09 m<sup>2</sup></td>\r\n<td>3 cm</td>\r\n<td>30 x 16,5 cm</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<th>1 КОМПЛЕКТ</th>\r\n<th>1,18 m<sup>2</sup></th>\r\n<th> </th>\r\n<th> </th>\r\n<th>1400 руб.</th>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'WDS575165, WDS601654A, WDS601654B, WDS601654C, WDS601654D, WDS30165, WDS27165, WDS601654A/B/C/D', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:18:20', '2020-11-30 19:33:52', '', '', '', '274522e520edb4978e988fa78eb48e99.jpg', null, null, null, '7', null, 'СЕРИЯ WDS - ДЕРЕВО', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('8', null, null, '7', '', 'Серия RVS', 'rvs-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Размер элементов</th>\r\n<th>Общая площадь</th>\r\n<th>Толщина изделий</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>RVS6045</td>\r\n<td>310 x 150 mm</td>\r\n<td>0,27 m<sup>2</sup></td>\r\n<td>25 мм</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>RVS5040</td>\r\n<td>500 x 120 mm</td>\r\n<td>0,2 m<sup>2</sup></td>\r\n<td>25 мм</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>RVS5025</td>\r\n<td>500 x 60 mm</td>\r\n<td>0,125 m<sup>2</sup></td>\r\n<td>25 мм</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', 'RVS6045, RVS5040, RVS5025', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:18:57', '2020-11-30 19:23:39', '', '', '', '6993e52fa4190e37d14de16a3b5f6203.jpg', null, null, null, '8', null, '', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('9', null, null, '7', '', 'Серия SL', 'sl-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Формат изделий</th>\r\n<th>Общая площадь</th>\r\n<th>Толщина изделий</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>SL9060</td>\r\n<td>I- 390 x 195 mm , II - 195 x 195 mm, III - 195 x 95 mm</td>\r\n<td>0,54 m<sup>2</sup></td>\r\n<td>25 мм</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<h3>SL - ФОРМЫ ДЛЯ УГЛОВЫХ ЭЛЕМЕНТОВ</h3>\r\n<p><img src=\"/uploads/image/9efc18e144825d8d0040660d0a96eafc.jpg\" alt=\"9efc18e144825d8d0040660d0a96eafc.jpg\" /></p>\r\n<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Размер изделия</th>\r\n<th>Площадь изделия</th>\r\n<th>Кол-во изделий в 1 форме</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>SLC604010 Полиуретановая вкладка</td>\r\n<td>390 x 95 x 195 mm</td>\r\n<td>0,58 погонных метра угла</td>\r\n<td>3 штуки</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>SLC604010M Форма с металлическим корпусом</td>\r\n<td>390 x 95 x 195 mm</td>\r\n<td>0,58 погонных метра угла</td>\r\n<td>3 штуки</td>\r\n<td> </td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>', '', 'SL9060, SLC604010 Полиуретановая вкладка, SLC604010M Форма с металлическим корпусом', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:19:34', '2020-11-30 19:35:22', '', '', '', '420b0f88e26f314d3c35bdc134eda3c6.jpg', null, null, null, '9', null, 'СЕРИЯ SL - СЛАНЕЦ', '', '', '', '', '0', '0.000', '1', '4');
INSERT INTO `yupe_store_product` VALUES ('10', null, null, '8', '', 'Серия OBR', 'obr-series', '0.000', null, null, '<table class=\"forms_spec_table\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Размер элементов</th>\r\n<th>Толщина изделия</th>\r\n<th>Общая площадь</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>OBR6040</td>\r\n<td>285 x 65 mm</td>\r\n<td>15 mm</td>\r\n<td>0,2 m<sup>2</sup></td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p><img src=\"/uploads/image/a36e2391ab3ef63618de5ad8146dbc81.jpg\" alt=\"a36e2391ab3ef63618de5ad8146dbc81.jpg\" /></p>\r\n<h3>OBR - ФОРМЫ УГЛОВЫХ ЭЛЕМЕНТОВ</h3>\r\n<p><img src=\"/uploads/image/48dc7365c0cb86e85faf34afab6100e6.jpg\" alt=\"48dc7365c0cb86e85faf34afab6100e6.jpg\" /></p>\r\n<table class=\"forms_spec_table\" style=\"width:100%;\">\r\n<tbody>\r\n<tr>\r\n<th>Индекс формы</th>\r\n<th>Формат изделия</th>\r\n<th>Толщина изделия</th>\r\n<th>Производимость формы</th>\r\n<th>Цена</th>\r\n</tr>\r\n<tr>\r\n<td>OBRC603015 - Полиуретановая вкладка</td>\r\n<td>285 x 65 x 150 mm</td>\r\n<td>15 mm</td>\r\n<td>0,5 погонных метра угла</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n<tr>\r\n<td>OBRC603015M - Форма с металлическим корпусом</td>\r\n<td>285 x 65 x 150 mm</td>\r\n<td>15 mm</td>\r\n<td>0,5 погонных метра угла</td>\r\n<td>1400 руб.</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p> </p>', '', 'OBR6040, OBRC603015 - Полиуретановая вкладка, OBRC603015M - Форма с металлическим корпусом', '0', null, null, null, null, null, '1', '1', '2020-11-28 13:50:30', '2020-11-30 19:35:57', '', '', '', 'd8d4c391e0027b739ad536d0da19be38.jpg', null, null, null, '10', null, '', '', '', '', '', '0', '0.000', '1', '5');

-- ----------------------------
-- Table structure for `yupe_store_product_attribute_value`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_attribute_value`;
CREATE TABLE `yupe_store_product_attribute_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `number_value` double DEFAULT NULL,
  `string_value` varchar(250) DEFAULT NULL,
  `text_value` text,
  `option_value` int(11) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `yupe_fk_product_attribute_product` (`product_id`),
  KEY `yupe_fk_product_attribute_attribute` (`attribute_id`),
  KEY `yupe_fk_product_attribute_option` (`option_value`),
  KEY `yupe_ix_product_attribute_number_value` (`number_value`),
  KEY `yupe_ix_product_attribute_string_value` (`string_value`),
  CONSTRAINT `yupe_fk_product_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE,
  CONSTRAINT `yupe_fk_product_attribute_option` FOREIGN KEY (`option_value`) REFERENCES `yupe_store_attribute_option` (`id`) ON DELETE CASCADE,
  CONSTRAINT `yupe_fk_product_attribute_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_attribute_value
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_badge`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_badge`;
CREATE TABLE `yupe_store_product_badge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `badge_id` int(11) DEFAULT NULL COMMENT 'Id Badge',
  `product_id` int(11) DEFAULT NULL COMMENT 'Id продукта',
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_badge_badge_id` (`badge_id`),
  KEY `fk_yupe_store_product_badge_product_id` (`product_id`),
  CONSTRAINT `fk_yupe_store_product_badge_badge_id` FOREIGN KEY (`badge_id`) REFERENCES `yupe_store_badge` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_badge_product_id` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_badge
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_category`;
CREATE TABLE `yupe_store_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_store_product_category_product_id` (`product_id`),
  KEY `ix_yupe_store_product_category_category_id` (`category_id`),
  CONSTRAINT `fk_yupe_store_product_category_category` FOREIGN KEY (`category_id`) REFERENCES `yupe_store_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_category_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_category
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_formimage`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_formimage`;
CREATE TABLE `yupe_store_product_formimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(255) NOT NULL COMMENT 'Изображение',
  `title` varchar(255) NOT NULL COMMENT 'Название изображения',
  `alt` varchar(255) NOT NULL COMMENT 'Alt изображения',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_formimage_product_id` (`product_id`),
  CONSTRAINT `fk_yupe_store_product_formimage_product_id` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_formimage
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_image`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_image`;
CREATE TABLE `yupe_store_product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `alt` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_yupe_store_product_image_product` (`product_id`),
  KEY `fk_yupe_store_product_image_group` (`group_id`),
  CONSTRAINT `fk_yupe_store_product_image_group` FOREIGN KEY (`group_id`) REFERENCES `yupe_store_product_image_group` (`id`) ON DELETE NO ACTION ON UPDATE SET NULL,
  CONSTRAINT `fk_yupe_store_product_image_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_image
-- ----------------------------
INSERT INTO `yupe_store_product_image` VALUES ('1', '1', '3007f4c7b65d9ae696a7399447635cc9.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('2', '1', '2ef32cb02a21ec97c63cd424addc17cd.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('3', '1', '675d3abc6a6edd310cd71a590c01f0ac.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('4', '10', 'c5e1430cb3d4843574e13afde5b48c73.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('5', '10', 'faabd0cc97fef4ae0517b5966e75d936.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('6', '10', '7283b8c040eeef88fa9c3ddfbf765890.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('7', '10', 'eb8a24d00465570ceb26b430c425ab80.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('8', '10', '93164ad9010b61924a5edf692cf984f1.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('9', '10', 'f2e2efb26a3e8a41f3f437b745e7ccef.jpg', 'OBR6040', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('10', '5', '120d4afc6f47f51816fc18ae245da583.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('11', '5', 'f2a7aff7e2e859b19a350da2c0b518c1.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('12', '4', '8bb2e84ce308f86f284bf6a4e71b19db.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('13', '4', '77c628f89597db527ef1221e36424032.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('14', '3', 'bddc84aa59b906326c29f882d620f71c.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('15', '3', '930806f07129c57ffa38ee90b1c59871.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('16', '2', '1eb6c0889361014afa282a6119616ab3.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('17', '2', '5fa0cad5ccdcce4d3ea2ff0d91773b10.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('18', '6', '31f5713c29bc1d04cb3fa92f2750a6ff.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('19', '6', '6b75a2a3488db598022d0eb273d171c9.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('22', '2', 'a872b87e1ce3c363e16d99f89bca5b8f.jpg', 'RKS6060A', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('23', '2', '709c25a91b0a60bd2c90e3178b685ca9.jpg', 'RKS6060B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('24', '2', '5b6e7160cc27c65bed15519e3f9117ba.jpg', 'RKS6060C', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('25', '3', 'a4a02679c6fecc969117a2cee4f479ac.jpg', 'TVD5050A', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('26', '3', 'd6114247ecf2ab74676dff4e9219aba8.jpg', 'TVD5050B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('27', '3', 'd028bace1392e015e7e98181039da3e0.jpg', 'TVD5050C', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('28', '3', 'a7f3c01d0ddef70018b13134ab483f2f.jpg', 'TVD5050D', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('29', '3', '8d49d9c8d077ba1a272611e30b9770c0.jpg', 'TVD5050E', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('30', '3', 'be0ac97ee0ca3d285b54e4f6ff31fc34.jpg', 'TVD5050F', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('31', '3', '50292d598c1a868bec3f4897df5fcb68.jpg', 'TVD5050G', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('32', '3', '4d4aee9d19db1eab6863dbb4c6d5b48d.jpg', 'TVD5050H', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('33', '4', '7c7d2abe96222cfc6bb6dc6763a7432e.jpg', 'BSL5045A', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('34', '4', '247773307eaf8859e0ac72999a7cb353.jpg', 'BSL5045B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('35', '4', '1d1c63dc90f96e2d48004c206092a0de.jpg', 'BSL5045C', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('36', '4', '309ce88aa3d8a0e3e174be6fcb76aff9.jpg', 'BSL5045D', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('37', '4', '6e8bebb446809f93c53386a67bb691f6.jpg', 'BSL4520 ½', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('38', '4', 'c0b423f9e0ac441e5b0b396bbb185ed3.jpg', 'BSL4520 ¾', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('39', '5', '825f32634706dff4516c01a633e1c578.jpg', 'KVR5040A', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('40', '5', '9d112d6f427812a7f315b6885428c046.jpg', 'KVR5040B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('41', '5', '95b84ccfa76bdd778f9012600c16364e.jpg', 'KVR5040C', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('42', '6', '61be2be09b0ed03d3d66381b55dcf8ae.jpg', 'PBK6050A', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('43', '6', '01bda119efd3c63793cba4ec94267553.jpg', 'PBK6050B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('44', '6', '5393a8f0315a156454613f085de5bcff.jpg', 'PBK6050D', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('45', '6', '83734c7d469741d7d145646679f57a84.jpg', 'PBKC6045B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('46', '7', 'ac3a8f14ca3ffb41aa53e7a937d15b37.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('47', '7', '37703a72e2f2d95fd8082bb0bd604cd4.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('48', '7', '973fc01018e6c225326816620debe169.jpg', 'WDS575165', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('49', '7', '330ae749a9003426891c8432fa4c3749.jpg', 'WDS601654A', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('50', '7', '8a06c1ddd0cb9344fc75fc89d70cc2ba.jpg', 'WDS601654B', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('51', '7', '4bee8b4fab77574947aaa66d0b67e725.jpg', 'WDS601654C', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('52', '7', '873784c65f5341224e29b757e1d8a006.jpg', 'WDS601654D', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('53', '7', '232b6fb4cfef7a702d04129a4ca8b4bc.jpg', 'WDS30165', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('54', '7', '3e3b5ad51da5a68109800b14121cfada.jpg', 'WDS27165', '', '2');
INSERT INTO `yupe_store_product_image` VALUES ('55', '9', 'fb211f32398a1bd2fad344c32da24050.jpg', '', '', '1');
INSERT INTO `yupe_store_product_image` VALUES ('56', '9', '8666e7088996bbef31ee57b2e3bdc815.jpg', '', '', '1');

-- ----------------------------
-- Table structure for `yupe_store_product_image_group`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_image_group`;
CREATE TABLE `yupe_store_product_image_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_image_group
-- ----------------------------
INSERT INTO `yupe_store_product_image_group` VALUES ('1', 'Основные фото (доп. к главной)');
INSERT INTO `yupe_store_product_image_group` VALUES ('2', 'Дополнительные фото(формы)');

-- ----------------------------
-- Table structure for `yupe_store_product_link`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_link`;
CREATE TABLE `yupe_store_product_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) DEFAULT NULL,
  `product_id` int(11) NOT NULL,
  `linked_product_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_product` (`product_id`,`linked_product_id`),
  KEY `fk_yupe_store_product_link_linked_product` (`linked_product_id`),
  KEY `fk_yupe_store_product_link_type` (`type_id`),
  CONSTRAINT `fk_yupe_store_product_link_linked_product` FOREIGN KEY (`linked_product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_link_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_product_link_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_link
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_product_link_type`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_link_type`;
CREATE TABLE `yupe_store_product_link_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_product_link_type_code` (`code`),
  UNIQUE KEY `ux_yupe_store_product_link_type_title` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_link_type
-- ----------------------------
INSERT INTO `yupe_store_product_link_type` VALUES ('1', 'similar', 'Похожие');
INSERT INTO `yupe_store_product_link_type` VALUES ('2', 'related', 'Сопутствующие');

-- ----------------------------
-- Table structure for `yupe_store_product_variant`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_product_variant`;
CREATE TABLE `yupe_store_product_variant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_value` varchar(255) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `type` tinyint(4) NOT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `position` int(11) NOT NULL DEFAULT '1',
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_yupe_store_product_variant_product` (`product_id`),
  KEY `idx_yupe_store_product_variant_attribute` (`attribute_id`),
  KEY `idx_yupe_store_product_variant_value` (`attribute_value`),
  CONSTRAINT `fk_yupe_store_product_variant_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_product_variant_product` FOREIGN KEY (`product_id`) REFERENCES `yupe_store_product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_product_variant
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_type`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_type`;
CREATE TABLE `yupe_store_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_store_type_name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_type
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_store_type_attribute`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_store_type_attribute`;
CREATE TABLE `yupe_store_type_attribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`attribute_id`),
  KEY `fk_yupe_store_type_attribute_attribute` (`attribute_id`),
  CONSTRAINT `fk_yupe_store_type_attribute_attribute` FOREIGN KEY (`attribute_id`) REFERENCES `yupe_store_attribute` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_store_type_attribute_type` FOREIGN KEY (`type_id`) REFERENCES `yupe_store_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_store_type_attribute
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_user_tokens`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_tokens`;
CREATE TABLE `yupe_user_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `expire_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_yupe_user_tokens_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_user_tokens_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_tokens
-- ----------------------------
INSERT INTO `yupe_user_tokens` VALUES ('8', '2', 'DBDlqYtk_dyaU2IXUbKl_r4~j~7WhZa4', '4', '0', '2020-11-28 00:12:41', '2020-11-28 00:12:41', '127.0.0.1', '2020-12-05 00:12:41');
INSERT INTO `yupe_user_tokens` VALUES ('9', '1', 'kMgYnNKSvu0fowgel0JnC7gE3jkethWw', '4', '0', '2020-11-30 18:52:36', '2020-11-30 18:52:36', '127.0.0.1', '2020-12-07 18:52:36');

-- ----------------------------
-- Table structure for `yupe_user_user`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user`;
CREATE TABLE `yupe_user_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `update_time` datetime NOT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(250) DEFAULT NULL,
  `last_name` varchar(250) DEFAULT NULL,
  `nick_name` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `birth_date` date DEFAULT NULL,
  `site` varchar(250) NOT NULL DEFAULT '',
  `about` varchar(250) NOT NULL DEFAULT '',
  `location` varchar(250) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `access_level` int(11) NOT NULL DEFAULT '0',
  `visit_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `avatar` varchar(150) DEFAULT NULL,
  `hash` varchar(255) NOT NULL DEFAULT '50443789b000cb912ae2c026e7e117600.90330600 1545824979',
  `email_confirm` tinyint(1) NOT NULL DEFAULT '0',
  `phone` char(30) DEFAULT NULL,
  `super_user` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_user_user_nick_name` (`nick_name`),
  UNIQUE KEY `ux_yupe_user_user_email` (`email`),
  KEY `ix_yupe_user_user_status` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user
-- ----------------------------
INSERT INTO `yupe_user_user` VALUES ('1', '2018-12-26 17:50:44', '', '', '', 'admin', 'nariman-abenov@mail.ru', '0', null, '', '', '', '1', '1', '2020-11-30 18:52:35', '2018-12-26 17:50:44', null, '$2y$13$n8zKhyL8z0DR9yUE1HmNlu.QUR6GwqIuebNFXyeHzb2pBqF3DcNci', '1', null, '1');
INSERT INTO `yupe_user_user` VALUES ('2', '2020-11-28 00:12:08', '', '', '', 'administrator', 'administrator@site.ru', '2', null, '', '', '', '1', '1', '2020-11-28 00:12:40', '2020-11-28 00:10:19', null, '$2y$13$wqZifz5Y4ggzlnEgEfrGx.6HyKtRJ.5kgW4lXeDUJqSA2yv9NFGji', '1', '', '0');

-- ----------------------------
-- Table structure for `yupe_user_user_auth_assignment`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_assignment`;
CREATE TABLE `yupe_user_user_auth_assignment` (
  `itemname` char(64) NOT NULL,
  `userid` int(11) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  KEY `fk_yupe_user_user_auth_assignment_user` (`userid`),
  CONSTRAINT `fk_yupe_user_user_auth_assignment_item` FOREIGN KEY (`itemname`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_assignment_user` FOREIGN KEY (`userid`) REFERENCES `yupe_user_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_assignment
-- ----------------------------
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin', '1', null, null);
INSERT INTO `yupe_user_user_auth_assignment` VALUES ('admin', '2', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item`;
CREATE TABLE `yupe_user_user_auth_item` (
  `name` char(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`),
  KEY `ix_yupe_user_user_auth_item_type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item
-- ----------------------------
INSERT INTO `yupe_user_user_auth_item` VALUES ('admin', '2', 'Admin', null, null);

-- ----------------------------
-- Table structure for `yupe_user_user_auth_item_child`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_user_user_auth_item_child`;
CREATE TABLE `yupe_user_user_auth_item_child` (
  `parent` char(64) NOT NULL,
  `child` char(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `fk_yupe_user_user_auth_item_child_child` (`child`),
  CONSTRAINT `fk_yupe_user_user_auth_item_child_child` FOREIGN KEY (`child`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_yupe_user_user_auth_itemchild_parent` FOREIGN KEY (`parent`) REFERENCES `yupe_user_user_auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_user_user_auth_item_child
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_video`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_video`;
CREATE TABLE `yupe_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `code` text NOT NULL COMMENT 'Код видео',
  `image` varchar(255) DEFAULT NULL COMMENT 'Изображение(миниатюра)',
  `status` int(11) DEFAULT '0' COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  `category_id` int(11) DEFAULT NULL COMMENT 'Категория',
  `code_vimeo` text,
  `is_footer` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_video
-- ----------------------------
INSERT INTO `yupe_video` VALUES ('1', 'Видео 1', '', '842c7128c35ca1f457919a5e7798111b.jpg', '1', '1', null, 'https://vimeo.com/213513378', '1');
INSERT INTO `yupe_video` VALUES ('2', 'Видео 2', '', '47df63f6837ef04d19a1ac4fd2c65a5e.jpg', '1', '2', null, 'https://vimeo.com/290545590', '1');
INSERT INTO `yupe_video` VALUES ('3', 'Видео 3', '', '91905ef921866b961b00cdd055b29cef.jpg', '1', '3', null, 'https://vimeo.com/190233533', '1');
INSERT INTO `yupe_video` VALUES ('4', 'MAGICRETE s.r.o.', 'https://www.youtube.com/watch?v=c0PRt2tTTO8', null, '1', '4', null, '', '0');
INSERT INTO `yupe_video` VALUES ('5', 'Magicrete Showcase At International Construction Trade Fair', 'https://www.youtube.com/watch?v=JRlU1Up-214', null, '1', '5', null, '', '0');

-- ----------------------------
-- Table structure for `yupe_video_category`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_video_category`;
CREATE TABLE `yupe_video_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT 'Название',
  `status` int(11) DEFAULT '0' COMMENT 'Статус',
  `position` int(11) DEFAULT NULL COMMENT 'Сортировка',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_video_category
-- ----------------------------

-- ----------------------------
-- Table structure for `yupe_yupe_settings`
-- ----------------------------
DROP TABLE IF EXISTS `yupe_yupe_settings`;
CREATE TABLE `yupe_yupe_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_id` varchar(100) NOT NULL,
  `param_name` varchar(100) NOT NULL,
  `param_value` varchar(500) NOT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ux_yupe_yupe_settings_module_id_param_name_user_id` (`module_id`,`param_name`,`user_id`),
  KEY `ix_yupe_yupe_settings_module_id` (`module_id`),
  KEY `ix_yupe_yupe_settings_param_name` (`param_name`),
  KEY `fk_yupe_yupe_settings_user_id` (`user_id`),
  CONSTRAINT `fk_yupe_yupe_settings_user_id` FOREIGN KEY (`user_id`) REFERENCES `yupe_user_user` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yupe_yupe_settings
-- ----------------------------
INSERT INTO `yupe_yupe_settings` VALUES ('1', 'yupe', 'siteDescription', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('2', 'yupe', 'siteName', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('3', 'yupe', 'siteKeyWords', '', '2018-12-26 17:51:10', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('4', 'yupe', 'email', 'nariman-abenov@mail.ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('5', 'yupe', 'theme', 'default', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('6', 'yupe', 'backendTheme', '', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('7', 'yupe', 'defaultLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('8', 'yupe', 'defaultBackendLanguage', 'ru', '2018-12-26 17:51:10', '2018-12-26 17:51:10', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('9', 'homepage', 'mode', '2', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('10', 'homepage', 'target', '1', '2019-01-14 09:39:38', '2019-01-14 09:39:41', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('11', 'homepage', 'limit', '', '2019-01-14 09:39:38', '2019-01-14 09:39:38', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('12', 'page', 'editor', 'tinymce5', '2020-05-22 09:50:05', '2020-08-26 11:23:26', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('13', 'page', 'mainCategory', '', '2020-05-22 09:50:05', '2020-05-22 09:50:05', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('14', 'yupe', 'coreCacheTime', '3600', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('15', 'yupe', 'uploadPath', 'uploads', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('16', 'yupe', 'editor', 'tinymce5', '2020-08-26 10:12:15', '2020-11-26 21:39:02', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('17', 'yupe', 'availableLanguages', 'ru,uk,en,zh', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('18', 'yupe', 'allowedIp', '', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('19', 'yupe', 'hidePanelUrls', '0', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('20', 'yupe', 'logo', 'images/logo.png', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('21', 'yupe', 'allowedExtensions', 'gif, jpeg, png, jpg, zip, rar, doc, docx, xls, xlsx, pdf', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('22', 'yupe', 'mimeTypes', 'image/gif,image/jpeg,image/png,application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/zip,application/x-rar,application/x-rar-compressed, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('23', 'yupe', 'maxSize', '5242880', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('24', 'yupe', 'defaultImage', '/images/nophoto.jpg', '2020-08-26 10:12:15', '2020-08-26 10:12:15', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('25', 'user', 'avatarMaxSize', '5242880', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('26', 'user', 'avatarExtensions', 'jpg,png,gif,jpeg', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('27', 'user', 'defaultAvatarPath', 'images/avatar.png', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('28', 'user', 'avatarsDir', 'avatars', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('29', 'user', 'showCaptcha', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('30', 'user', 'minCaptchaLength', '3', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('31', 'user', 'maxCaptchaLength', '6', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('32', 'user', 'minPasswordLength', '8', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('33', 'user', 'autoRecoveryPassword', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('34', 'user', 'recoveryDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('35', 'user', 'registrationDisabled', '0', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('36', 'user', 'notifyEmailFrom', 'no-reply@dcmr.ru', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('37', 'user', 'logoutSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('38', 'user', 'loginSuccess', '/', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('39', 'user', 'accountActivationSuccess', '/user/account/login', '2020-08-26 10:13:59', '2020-08-26 10:13:59', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('40', 'user', 'accountActivationFailure', '/user/account/registration', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('41', 'user', 'loginAdminSuccess', '/yupe/backend/index', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('42', 'user', 'registrationSuccess', '/user/account/login', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('43', 'user', 'sessionLifeTime', '7', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('44', 'user', 'usersPerPage', '20', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('45', 'user', 'emailAccountVerification', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('46', 'user', 'badLoginCount', '3', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('47', 'user', 'phoneMask', '+7(999) 999-99-99', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('48', 'user', 'phonePattern', '/\\+7\\(\\d{3}\\) \\d{3}\\-\\d{2}\\-\\d{2}$/', '2020-08-26 10:14:00', '2020-08-26 10:14:00', '1', '1');
INSERT INTO `yupe_yupe_settings` VALUES ('49', 'user', 'generateNickName', '1', '2020-08-26 10:14:00', '2020-08-26 10:14:10', '1', '1');
