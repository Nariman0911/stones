$(document).ready(function() {
    /**********************************************/
    /*
     * Слайдеры
    */
    /* Главный слайд */
    if($('div').hasClass('main-slidebox-carousel')){
        var slidebox = $('.main-slidebox-carousel');

        slidebox.slick({
            fade: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                       arrows: false,
                       dots: true,
                    }
                },
            ]
        });
    }
    /**********************************************/

    /* Карусель видео */
    if($('div').hasClass('video-box-carousel')){
        var videobox = $('.video-box-carousel');

        videobox.slick({
            fade: false,
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            autoplay: false,
            autoplaySpeed: 5000,
            dots: false,
            arrows: true,
            responsive: [
                {
                    breakpoint: 769,
                    settings: {
                       arrows: false,
                       dots: true,
                    }
                },
            ]
        });
    }
    /**********************************************/
});