$(document).ready(function() {
    /*
     * Menu при адаптации
    */
    // Событие открывания меню на телефонах
    $('.js-menu-mobileNav').on('click', function() {
        if($(this).hasClass('active')){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(this).removeClass('active');
            $(".mobileNav, .mobileNav-box, .mobileNav__icon-close").removeClass('active');
        } else {
            $('html').addClass('htmlmenu');
            $('body').addClass('bodymenu');
            $(this).addClass('active');
            $(".mobileNav").addClass('active');
            $(".mobileNav-box").addClass('active');
            $(".mobileNav__icon-close").addClass('active');
        }
        return false;
    });
    $('.mobileNav').on('click', function(e){
        console.log($(e.target));
        if($(e.target).hasClass('mobileNav__icon-close') || $(e.target).parents('.mobileNav__icon-close').length > 0 || $(e.target).hasClass('mobileNav')){
            $('html').removeClass('htmlmenu');
            $('body').removeClass('bodymenu');
            $(".js-menu-mobileNav, .mobileNav, .mobileNav-box, .mobileNav__icon-close").removeClass('active');
        }
    });
    var menumain = $('.header .menu .menu-main').html();
    $('.mobileNav .menu-mobile').append(menumain);

    $('.mobileNav .menu-mobile li ul').wrap('<div class="menu-mobile-wrapper"></div>');
    $('.mobileNav .menu-mobile li a').click(function(){
        var parent = $(this).parent();
        if(parent.hasClass('submenuItem')){
            parent.find(".menu-mobile-wrapper:first").addClass('active').prepend('<a class="prevLink" href="#">Назад</a>');
            return false;
        }
    });

    $('.mobileNav .menu-mobile li.listItemParent').each(function(i){
        if($(this).hasClass('submenuItem')){
            var el = $(this).find('a:first');
            var link_href = el.attr('href');
            link = "<a class='listItem-nav__link' href='"+link_href+"'>"+el.html()+"</a>";
            $(this).find(".menu-mobile-wrapper ul:eq(0)").prepend("<li class='listItem listItem-nav'>" + link + "</li>");
            appendLi($(this).find(".menu-mobile-wrapper ul:eq(0) > li.submenuItem"), link);
        }
    });

    function appendLi(element, link_prev){
        var link_res;
        var count = 1;
        element.each(function(index){
            link_res = link_prev;
            if($(this).hasClass('listItemFloor')){
                link_prev = '<a class="listItem-nav__link" href="#">Магазины</a>'
            }
            if($(this).hasClass('submenuItem')){
                var el = $(this).find('a:first');
                var link_href = el.attr('href');
                var link_active = "<a class='listItem-nav__link' href='"+link_href+"'>"+el.html()+"</a>";
                link_res += link_active;
                $(this).find(".menu-mobile-wrapper ul:eq(0)").prepend("<li class='listItem listItem-nav'>" + link_prev + link_active + "</li>");
                appendLi($(this).find(".menu-mobile-wrapper ul:eq(0) > li.submenuItem"), link_res);
            }
        })
    }

    $(document).delegate('.mobileNav .menu-mobile .prevLink', 'click', function(){
        var el = $(this);
        var parent = $(this).parent();
        if(parent.hasClass('active')){
            parent.removeClass('active');
            setTimeout(function(){
                el.remove();
            }, 600);
        }
        return false;
    });
    /**********************************************/
});