$(document).ready(function() {
    /**********************************************/
    /*
     *** Кнопка больше информации СЕО-ТЕКСТ ***
    */
    $('.js-seo-txt-section-link').on('click', function(){
        var parent = $('.js-seo-txt-section');
        var txt = $(this).data('text');
        var txt_hidden = $(this).html();
        var height = parent.find('.js-seo-txt-more').height();
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            parent.removeClass('active').attr('style', '');
            $(this).html(txt).data('text', txt_hidden);
        } else {
            $(this).addClass('active');
            parent.addClass('active').css('max-height', height);
            $(this).html(txt).data('text', txt_hidden);
        }
        return false;
    });
    /**********************************************/
});


// @prepros-append "./lib/slick.js"
// @prepros-append "./lib/jquery.inputmask.js"
// @prepros-append "./lib/_form.js"
// #@prepros-append "./lib/_menu.js"
// @prepros-append "./lib/_carousel.js"
// @prepros-append "./lib/fancybox.js"