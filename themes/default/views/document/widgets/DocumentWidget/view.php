<div class="arenda-template-list fl fl-wr-w">
	<?php foreach ($model as $key => $template) : ?>
	  	<div class="arenda-template-list__item">
	  		<a href="<?= $template->getDocument(); ?>">
			    <div class="arenda-template-list__img">
			    	<?= CHtml::image($template->getImageUrl(), '', ['class'=>''])?>
			    </div>
			    <div class="arenda-template-list__info">
			      	<div class="arenda-template-list__name">
			        	<?= $template->name; ?>
			      	</div>
			    </div>
		    </a>
	  	</div>
	<?php endforeach ?>
</div>