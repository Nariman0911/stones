<div class="menu-catalog-section">
    <?php $this->widget('application.modules.store.widgets.CatalogWidget', [
        'view' => 'catalog-menu',
        'is_menu' => true
    ]); ?>
</div>

<?php $this->widget('application.modules.video.widgets.VideoWidget', [
    'is_footer' => true,
    'view' => 'view-carousel'
]); ?>

<footer>
    <div class="content">
        <div class="footer fl fl-wr-w">
            <div class="footer__item footer__item_menu">
                <?php if(Yii::app()->hasModule('menu')): ?>
                    <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                        'view' => 'footermenu', 
                        'name' => 'top-menu'
                    ]); ?>
                <?php endif; ?>
            </div>
            <div class="footer__item footer__item_contact">
                <div class="footer__heading">НАШИ КОНТАКТЫ</div>
                <div class="footer-contact">
                    <div class="footer-contact__item footer-contact__item_loc">
                        <div class="footer-contact__heading">Адрес:</div>
                        <div class="footer-contact__location">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 3
                            ]); ?>
                        </div>
                    </div>
                    <div class="footer-contact__item footer-contact__item_phone">
                        <div class="footer-contact__heading">Телефон:</div>
                        <div class="footer-contact__phone">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 1
                            ]); ?>
                        </div>
                    </div>
                    <div class="footer-contact__item footer-contact__item_email">
                        <div class="footer-contact__email">
                            <?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', [
                                'id' => 2
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer__item footer__item_map">
                <div class="footer-map">
                    <iframe src="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 7]); ?>" frameborder="0" style="border: 0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>  
    </div>
    <div class="content">
        <div class="footer-bot fl fl-al-it-c">
            <div class="footer-copy">
                &copy; 2005-<?= date("Y"); ?> CONA.
            </div>
        </div>
    </div>
</footer>