<header class="<?= ($this->action->id=='index' && $this->id=='hp') ? 'header-home' : 'header-page'; ?>">
    <div class="header">
        <div class="content fl fl-al-it-c fl-ju-co-sp-b">
            <div class="header__logo header-logo">
                <a class="fl" href="/">
                    <?= CHtml::image($this->mainAssets . '/images/logo.svg', '', ['class' => 'logo-one']); ?>
                    <?= CHtml::image($this->mainAssets . '/images/logo.svg', '', ['class' => 'logo-two']); ?>
                    <?= CHtml::image($this->mainAssets . '/images/logo.svg', '', ['class' => 'logo-three']); ?>
                </a>
            </div>
            <div class="header__section fl fl-di-c fl-al-it-fl-e">
                <div class="header__menu">
                    <?php if(Yii::app()->hasModule('menu')): ?>
                        <?php $this->widget('application.modules.menu.widgets.MenuWidget', [
                            'view' => 'main', 
                            'name' => 'top-menu'
                        ]); ?>
                    <?php endif; ?>
                </div>
                <div class="header__search header-search">
                    <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="menu-catalog-section-abs">
        <div class="menu-catalog-section">
            <?php $this->widget('application.modules.store.widgets.CatalogWidget', [
                'view' => 'catalog-menu',
                'is_menu' => true
            ]); ?>
        </div>
    </div>
</header>