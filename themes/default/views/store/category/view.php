<?php
$mainAssets = Yii::app()->getTheme()->getAssetsUrl();
/* @var $category StoreCategory */

$this->title =  $category->getMetaTitle();
$this->description = $category->getMetaDescription();
$this->keywords =  $category->getMetaKeywords();
$this->canonical = $category->getMetaCanonical();

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog") => ['/store/product/index']];

$this->breadcrumbs = array_merge(
    $this->breadcrumbs,
    $category->getBreadcrumbs(false)
);

?>

<div class="page-header">
    <?php $this->widget('application.modules.slider.widgets.SliderWidget', [
        'storecategory_id' => $category->id
    ]); ?>
    <div class="page-header__heading">
        <div class="content">
            <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
            ]); ?>

            <h1><?= CHtml::encode($category->getTitle()); ?></h1>
        </div>
    </div>
</div>

<div class="page-content txt-style">
    <div class="content">
        
        <?= $category->description; ?>

        <?php $children = $category->children; ?>
        <div class="category-section <?= ($category->description) ? '' : 'category-section-2'?>">
            <?php if($children) : ?>
                <?php if($category->productCount > 0) : ?>
                    <?php $this->widget('application.modules.store.widgets.ProductWidget', [
                        'category_id'  => $category->id
                    ]); ?>
                <?php endif; ?>

                <?php foreach ($children as $key => $data) : ?>
                    <?php $this->widget('application.modules.store.widgets.ProductsFromCategoryWidget', [
                        'view'      => 'category-product',
                        'category'  => $data
                    ]); ?>
                <?php endforeach; ?>
            <?php else : ?>
                <?php 
                    $this->widget(
                        'application.components.MyListView',
                    [
                        'dataProvider' => $dataProvider,
                        'id' => 'product-box-listView',
                        'itemView' => '//store/product/_item',
                        'emptyText'=>'В данной категории нет товаров.',
                        'summaryText'=>"{count} тов.",
                        'template'=>'
                            {items}
                            {pager}
                        ',
                        'itemsCssClass' => 'product-box fl fl-wr-w',
                        'htmlOptions' => [
                            'class' => 'product-section'
                        ],
                        'ajaxUpdate'=>true,
                        'enableHistory' => false,
                        'pagerCssClass' => 'pagination-box',
                        'pager' => [
                            'header' => '',
                            'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                            'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                            'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                            'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                            'maxButtonCount' => 5,
                            'htmlOptions' => [
                                'class' => 'pagination'
                            ],
                        ]
                    ]
                ); ?>
            <?php endif; ?>
        </div>
    </div>
</div>
