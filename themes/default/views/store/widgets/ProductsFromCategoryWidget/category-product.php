<?php if ($products->itemCount): ?>
	<h2><?= $category->name; ?></h2>
	<div class="product-section">
		<div class="product-box fl fl-wr-w">
			<?php foreach ($products->getData() as $i => $product): ?>
				<?php $this->render('../../product/_item', ['data' => $product]) ?>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>