<?php if($models) : ?>
	<div class="content">
		<div class="catalog-home">
			<div class="catalog-box fl fl-wr-w">
				<?php foreach ($models as $key => $data) : ?>
					<div class="catalog-box__item">
						<a href="<?= $data->getCategoryUrl(); ?>">
							<div class="catalog-box__image">
								<picture>
									<source media="(min-width: 1px)" src="<?= $data->getImageUrlWebp(0, 0, false, null,'image'); ?>" type="image/webp">
									<source media="(min-width: 1px)" src="<?= $data->getImageNewUrl(0, 0, false, null,'image'); ?>" >

									<img src="<?= $data->getImageNewUrl(0, 0, false, null,'image'); ?>" alt="" />
								</picture>
							</div>
							<div class="catalog-box__info">
								<div class="catalog-box__name">
									<?= $data->name_short; ?>
								</div>
							</div>
						</a>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
<?php endif; ?>