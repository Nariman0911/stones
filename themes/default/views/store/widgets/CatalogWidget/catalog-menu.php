<?php if($models) : ?>
	<div class="content">
		<ul class="menu-catalog fl fl-al-it-c fl-ju-co-sp-b">
			<?php foreach ($models as $key => $data) : ?>
				<li>
					<a href="<?= $data->getCategoryUrl(); ?>">
						<?= $data->name_short; ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>