<form class="search-form" action="<?= Yii::app()->createUrl('/store/product/search')?>" method="GET">
    <div class="input-group search-input-group">
        <?= CHtml::textField(AttributeFilter::MAIN_SEARCH_QUERY_NAME, CHtml::encode(Yii::app()->getRequest()->getQuery(AttributeFilter::MAIN_SEARCH_QUERY_NAME)),
            [
                'class'        => 'form-control', 
                'autocomplete' => 'off',
                'placeholder'  => 'Искать на сайте'
            ]
        ); ?>
        <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <?= file_get_contents('.'. Yii::app()->getTheme()->getAssetsUrl() . '/images/icon/icon-search.svg'); ?>
            </button>
        </span>
    </div>
</form>