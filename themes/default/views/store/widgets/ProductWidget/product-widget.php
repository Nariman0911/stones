<?php if ($products): ?>
	<div class="product-section">
		<div class="product-box fl fl-wr-w">
			<?php foreach ($products as $i => $product): ?>
				<?php $this->render('../../product/_item', ['data' => $product]) ?>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif; ?>