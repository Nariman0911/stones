<div class="product-box__item">
    <div class="product-box__img box-style-img">
        <a href="<?= ProductHelper::getUrl($data); ?>">
            <picture>
                <source srcset="<?= $data->getImageUrlWebp(360, 260, true, null, 'image'); ?>" type="image/webp">
                <img src="<?= $data->getImageNewUrl(360, 260, true, null, 'image'); ?>" alt="">
            </picture>
        </a>
    </div>
    <div class="product-box__info">
        <a href="<?= ProductHelper::getUrl($data); ?>">
            <div class="product-box__name">
                <?= CHtml::encode($data->getName()); ?>
            </div>
        </a>
    </div>
</div>