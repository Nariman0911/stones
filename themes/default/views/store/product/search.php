<?php

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

/* @var $category StoreCategory */
$this->title = Yii::t("StoreModule.store", "Search");
$this->breadcrumbs = [
    Yii::t("StoreModule.store", "Catalog") => ['/store/product/index'],
    Yii::t("StoreModule.store", "Search"),
];
?>
<div class="page-header">
    <?php $this->widget('application.modules.slider.widgets.SliderWidget', [
        'storecategory_id' => 0
    ]); ?>
    <div class="page-header__heading">
        <div class="content">
            <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
            ]); ?>

            <h1><?= Yii::t("StoreModule.store", "Search"); ?></h1>
        </div>
    </div>
</div>

<div class="page-content txt-style">
    <div class="content">
        <div class="search-section">
            <h2><?= Yii::t("StoreModule.store", "Search"); ?></h2>
            <?php $this->widget('application.modules.store.widgets.SearchProductWidget'); ?>
        </div>

        <div class="category-section">
            <?php 
                $this->widget(
                    'application.components.MyListView',
                [
                    'dataProvider' => $dataProvider,
                    'id' => 'product-box-listView',
                    'itemView' => '_item',
                    'emptyText'=>'По данному запросу товаров не найдено!',
                    'summaryText'=>"{count} тов.",
                    'template'=>'
                        {items}
                        {pager}
                    ',
                    'itemsCssClass' => 'product-box fl fl-wr-w',
                    'htmlOptions' => [
                        'class' => 'product-section'
                    ],
                    'ajaxUpdate'=>true,
                    'enableHistory' => false,
                    'pagerCssClass' => 'pagination-box',
                    'pager' => [
                        'header' => '',
                        'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                        'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                        'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                        'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                        'maxButtonCount' => 5,
                        'htmlOptions' => [
                            'class' => 'pagination'
                        ],
                    ]
                ]
            ); ?>
        </div>
    </div>
</div>