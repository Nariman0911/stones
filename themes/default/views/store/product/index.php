<?php

$mainAssets = Yii::app()->getTheme()->getAssetsUrl();

/* @var $category StoreCategory */

$this->title = Yii::app()->getModule('store')->metaTitle ?: Yii::t('StoreModule.store', 'Catalog');
$this->description = Yii::app()->getModule('store')->metaDescription;
$this->keywords = Yii::app()->getModule('store')->metaKeyWords;

$this->breadcrumbs = [Yii::t("StoreModule.store", "Catalog")];
?>

<div class="page-header">
    <?php $this->widget('application.modules.slider.widgets.SliderWidget', [
        'storecategory_id' => 0
    ]); ?>
    <div class="page-header__heading">
        <div class="content">
            <?php $this->widget('application.components.MyTbBreadcrumbs', [
                'links' => $this->breadcrumbs,
            ]); ?>

            <h1><?= Yii::t("StoreModule.store", "Catalog"); ?></h1>
        </div>
    </div>
</div>

<div class="page-content page-catalog-home">
    <?php $this->widget('application.modules.store.widgets.CatalogWidget', [
        'view' => 'catalog-home'
    ]); ?>
</div>
