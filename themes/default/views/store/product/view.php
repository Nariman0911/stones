<?php

/* @var $product Product */

$this->title = $product->getMetaTitle();
$this->description = $product->getMetaDescription();
$this->keywords = $product->getMetaKeywords();
$this->canonical = $product->getMetaCanonical();

$mainAssets = Yii::app()->getModule('store')->getAssetsUrl();

$this->breadcrumbs = array_merge(
    [Yii::t("StoreModule.store", 'Catalog') => ['/store/product/index']],
    $product->category ? $product->category->getBreadcrumbs(true) : [],
    [CHtml::encode($product->name)]
);
?>
<div xmlns="http://www.w3.org/1999/html" itemscope itemtype="http://schema.org/Product">
    <div class="page-header">
        <?php $this->widget('application.modules.slider.widgets.SliderWidget', [
            'storecategory_id' => $product->category_id
        ]); ?>
        <div class="page-header__heading">
            <div class="content">
                <?php $this->widget('application.components.MyTbBreadcrumbs', [
                    'links' => $this->breadcrumbs,
                ]); ?>

                <h2><?= CHtml::encode($product->category->name_short); ?></h2>
            </div>
        </div>
    </div>

    <div class="page-content txt-style">
        <div class="content">
            <h1 itemprop="name"><?= CHtml::encode($product->getTitle()); ?></h1>
            <div class="product-media fl fl-ju-co-sp-b">
                <div class="product-media__item product-media__item_photo box-style-img">
                    <a data-fancybox="image" href="<?= StoreImage::product($product); ?>">
                        <picture>
                            <source srcset="<?= $product->getImageUrlWebp(675, 250, true, null, 'image'); ?>" type="image/webp">
                            <img src="<?= $product->getImageNewUrl(675, 250, true, null, 'image'); ?>" alt="" itemprop="image">
                        </picture>
                    </a>
                </div>
                <div class="product-media__item product-media__item_video">
                    <iframe src="<?= ($product->video->code) ? $product->video->getLinkYoutube() : $product->video->getLinkVimeo(); ?>" width="640" height="360" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                </div>
            </div>

            <?php $images = $product->getImages(); ?>
            <?php if($images) : ?>
                <div class="product-thumb-section">
                    <div class="image-thumb fl fl-wr-w">
                        <?php foreach ($images as $key => $image) : ?>
                            <?php if($image->group_id == 1) : ?>
                                <div class="image-thumb__item box-style-img">
                                    <a class="fl fl-al-it-c fl-ju-co-c" data-fancybox="image" href="<?= $image->getImageUrl(); ?>">
                                        <picture>
                                            <source srcset="<?= $image->getImageUrlWebp(205, 130, true, null, 'name'); ?>" type="image/webp">
                                            <img src="<?= $image->getImageNewUrl(205, 130, true, null, 'name'); ?>" alt="" itemprop="image">
                                        </picture>
                                    </a>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
            <?php endif; ?>

            <div class="product-view-info">
                <div class="formImage-thumb fl fl-wr-w">
                    <?php foreach ($images as $key => $image) : ?>
                        <?php if($image->group_id == 2) : ?>
                            <div class="formImage-thumb__item">
                                <a data-fancybox="image2" href="<?= $image->getImageUrl(); ?>">
                                    <div class="formImage-thumb__img">
                                        <picture>
                                            <source srcset="<?= $image->getImageUrlWebp(268, 166, false, null, 'name'); ?>" type="image/webp">
                                            <img src="<?= $image->getImageNewUrl(268, 166, false, null, 'name'); ?>" alt="" itemprop="image">
                                        </picture>
                                    </div>
                                </a>
                                <div class="formImage-thumb__info">
                                    <div class="formImage-thumb__name"><?= $image->title; ?></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>

                <div class="product-view-description" itemprop="description">
                    <?= $product->description; ?>
                </div>

                <div class="product-view-but fl fl-al-it-c fl-ju-co-c">
                    <?php if($product->document_id) : ?>
                        <a class="but but-animate-transform" target="_blank" href="<?= $product->document->getDocument(); ?>">Скачать каталог</a>
                    <?php endif; ?>
                    <a class="but but-animate-transform" data-toggle="modal" data-target="#productionModal" href="#">Отправить запрос</a>
                    <a class="but but-animate-transform" href="https://wa.me/<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 8]); ?>"><?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 8, 'view' => 'contentblock-name']); ?></a>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $fancybox = $this->widget(
    'gallery.extensions.fancybox3.AlFancybox', [
        'target' => '[data-fancybox="image2"]',
        'lang'   => 'ru',
        'config' => [
            'animationEffect' => "fade",
            'buttons' => [
                "zoom",
                "close",
            ]
        ],
    ]
); ?>

<!-- Отправить запрос -->
<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
    'id' => 'productionModal',
    'formClassName' => 'StandartForm',
    'buttonModal' => false,
    'titleModal' => 'Отправить запрос',
    'showCloseButton' => false,
    'isRefresh' => true,
    'showAttributeEmail' => true,
    'showAttributeComment' => true,
    'eventCode' => 'ostavit-zapros',
    'successKey' => 'ostavit-zapros',
    'modalHtmlOptions' => [
        'class' => 'modal-my',
    ],
    'formOptions' => [
        'htmlOptions' => [
            'class' => 'form-my',
        ]
    ],
    'modelAttributes' => [
        'theme' => 'Запрос на товар',
        'link' => "<a href='".ProductHelper::getUrl($product, true)."'>".$product->name."</a>",
    ],
]) ?>