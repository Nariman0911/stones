<?php $this->widget('application.modules.store.widgets.ProductsFromCategoryWidget', [
    'view'      => 'category-product',
	'category'  => $data,
	'limit'     => 3,
	'storeItem' => $this->storeItem,
]); ?>
