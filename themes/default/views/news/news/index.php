<?php
$this->title = Yii::app()->getModule('news')->metaTitle ?: Yii::t('NewsModule.news', 'News');
$this->description = Yii::app()->getModule('news')->metaDescription;
$this->keywords = Yii::app()->getModule('news')->metaKeyWords;

$this->breadcrumbs = [Yii::t('NewsModule.news', 'News')];
?>

<div class="page-content">
	<div class="content">
		<?php $this->widget('application.components.MyTbBreadcrumbs', [
            'links' => $this->breadcrumbs,
        ]); ?>

		<h1><?= Yii::t('NewsModule.news', 'News') ?></h1>

		<?php  $this->widget(
            'bootstrap.widgets.TbListView',
            [
                'dataProvider' => $dataProvider,
                'id' => 'news-box',
                'itemView' => '_item',
                'template'=>'
                    {items}
                    {pager}
                ',
                'itemsCssClass' => "news-box news-page",
                'htmlOptions' => [
                    // 'class' => 'news-box'
                ],
                'ajaxUpdate'=>true,
                'enableHistory' => false,
                // 'ajaxUrl'=>'GET',
                'pagerCssClass' => 'pagination-box',
                    'pager' => [
                    'header' => '',
                    'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                    'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                    // 'lastPageLabel'  => false,
                    // 'firstPageLabel' => false,
                    'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                    'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                    'maxButtonCount' => 5,
                    'htmlOptions' => [
                        'class' => 'pagination'
                    ],
                ]
            ]
        ); ?>
	</div>
</div>
