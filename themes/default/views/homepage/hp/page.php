<?php
/** @var Page $page */

if ($page->layout) {
    $this->layout = "//layouts/{$page->layout}";
}

$this->title = $page->meta_title ?: $page->title;
$this->breadcrumbs = [
    Yii::t('HomepageModule.homepage', 'Pages'),
    $page->title
];
$this->description = !empty($page->meta_description) ? $page->meta_description : Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = !empty($page->meta_keywords) ? $page->meta_keywords : Yii::app()->getModule('yupe')->siteKeyWords;

?>

<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
	'page_id' => $page->id
]); ?>

<?php $this->widget('application.modules.store.widgets.CatalogWidget', [
	'view' => 'catalog-home',
	'is_home' => true
]); ?>

<div class="content">
	<div class="main-onebox txt-style fl fl-wr-w">
		<div class="main-onebox__col">
			<?= $page->body; ?>
		</div>
		<div class="main-onebox__col">
			<?= $page->body_short; ?>
			<?php $instagramButLink = $page->getAttributeValue('instagram')['butLink']; ?>
			<?php $instagramImage = $page->getAttributeValue('instagram')['image']; ?>
			<?php if($instagramImage && $instagramButLink) : ?>
				<a class="main-onebox__instagram" href="<?= $instagramButLink ?>">
					<picture>
						<source media="(min-width: 1px)" src="<?= $page->geFieldImageWebp(0, 0, false, $instagramImage); ?>" type="image/webp">
						<source media="(min-width: 1px)" src="<?= $page->getFieldImageUrl(0, 0, false, $instagramImage); ?>" >

						<img src="<?= $page->getFieldImageUrl(0, 0, false, $instagramImage); ?>" alt="" />
					</picture>
				</a>
		<?php endif; ?>
		</div>
	</div>
</div>

