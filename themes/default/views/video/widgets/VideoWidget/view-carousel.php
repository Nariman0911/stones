<div class="video-section">
    <div class="content">
        <div class="video-box video-box-carousel slick-slider">
            <?php foreach ($models as $key => $item): ?>
                <div>
                    <?php Yii::app()->controller->renderPartial('//video/video/_video', ['data' => $item]) ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>