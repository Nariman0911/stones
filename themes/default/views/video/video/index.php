<?php
/**
* Отображение для video/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     http://yupe.ru
**/
$this->title = Yii::t('VideoModule.video', 'video');
$this->description = Yii::t('VideoModule.video', 'video');
$this->keywords = Yii::t('VideoModule.video', 'video');

$this->breadcrumbs = [
	Yii::t('VideoModule.video', 'video')
]; ?>

<div class="page-content">
    <div class="content">
        <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', [
                'links' => $this->breadcrumbs,
        ]); ?>

        <h1><?= Yii::t('VideoModule.video', 'video'); ?></h1>
        <div class="video-page">
            <?php foreach ($category as $key => $item) : ?>
                <?php if($item->video) : ?>
                    <div class="video-box fl fl-wr-w">
                        <?php foreach ($item->video(['order' => 'position DESC']) as $key => $data) : ?>
                            <?php Yii::app()->controller->renderPartial('_video', ['data' => $data]) ?>
                        <?php endforeach; ?>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </div>
    <?php /*$this->widget(
        'bootstrap.widgets.TbListView',
        [
            'dataProvider' => $dataProvider,
            'itemView'     => '_video',
            'template'     => "{items}\n{pager}",
            'itemsCssClass' => 'video-box',
            'ajaxUpdate'=>'true',
            'pagerCssClass' => 'pagination-box',
            'pager' => [
                'header' => '',
                'lastPageLabel' => '<i class="fa fa-angle-double-right" aria-hidden="true"></i>',
                'firstPageLabel' => '<i class="fa fa-angle-double-left" aria-hidden="true"></i>',
                'prevPageLabel' => '<i class="fa fa-angle-left" aria-hidden="true"></i>',
                'nextPageLabel' => '<i class="fa fa-angle-right" aria-hidden="true"></i>',
                'maxButtonCount' => 5,
                'htmlOptions' => [
                    'class' => 'pagination'
                ],
            ]
        ]
    );*/ ?>
    <?php $fancybox = $this->widget(
        'gallery.extensions.fancybox3.AlFancybox', [
            'target' => '[data-fancybox]',
            'lang'   => 'ru',
            'config' => [
                'animationEffect' => "fade",
                'buttons' => [
                    "zoom",
                    "close",
                ]
            ],
        ]
    ); ?>
</div>
