<?php $nophoto = Yii::app()->getTheme()->getAssetsUrl() . '/images/nophoto.jpg'; ?>
<div class="video-box__item">
    <a class="video-box-play" data-fancybox="iframe" href="<?= ($data->code) ? $data->code : $data->code_vimeo; ?>">
        <?php if($data->image) : ?>
            <div class="video-box__img box-style-img">
                <picture>
                    <source media="(min-width: 401px)" srcset="<?= $data->getImageUrlWebp(360, 200, true, $nophoto, 'image'); ?>" type="image/webp">
                    <source media="(min-width: 401px)" srcset="<?= $data->getImageNewUrl(360, 200, true, $nophoto, 'image'); ?>">

                    <source media="(min-width: 1px)" srcset="<?= $data->getImageUrlWebp(360, 200, true, $nophoto, 'image'); ?>" type="image/webp">
                    <source media="(min-width: 1px)" srcset="<?= $data->getImageNewUrl(360, 200, true, $nophoto, 'image'); ?>">

                    <img src="<?= $data->getImageNewUrl(360, 200, true, $nophoto, 'image'); ?>" alt="<?= CHtml::encode($data->name); ?>">
                </picture>
            </div>
        <?php else : ?>
            <iframe src="<?= ($data->code) ? $data->getLinkYoutube() : $data->getLinkVimeo(); ?>" width="640" height="360" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
        <?php endif; ?>
    </a>
</div>