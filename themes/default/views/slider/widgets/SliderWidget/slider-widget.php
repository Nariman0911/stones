<div class="main-slide-section">
	<div class="main-slidebox main-slidebox-carousel slick-slider">
		<?php if($models) : ?>
			<?php foreach ($models as $key => $data): ?>
			    <div class="main-slidebox__item">
					<div class="main-slidebox__img box-style-img">
		                <picture>
							<source media="(min-width: 1px)" src="<?= $data->getImageUrlWebp(0, 0, false, null,'image'); ?>" type="image/webp">
							<source media="(min-width: 1px)" src="<?= $data->getImageNewUrl(0, 0, false, null,'image'); ?>" >

							<img src="<?= $data->getImageNewUrl(0, 0, false, null,'image'); ?>" alt="" />
						</picture>
					</div>
			    </div>
			<?php endforeach; ?>
		<?php else : ?>
			<div class="main-slidebox__item">
				<div class="main-slidebox__img box-style-img">
	                <picture>
						<img src="<?= Yii::app()->getTheme()->getAssetsUrl() . '/images/main-1.jpg'; ?>" alt="" />
					</picture>
				</div>
		    </div>
		<?php endif; ?>
	</div>
</div>