<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-header">
	<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
		'page_id' => $model->id
	]); ?>
	<div class="page-header__heading">
		<div class="content">
			<?php $this->widget('application.components.MyTbBreadcrumbs', [
	            'links' => $this->breadcrumbs,
	        ]); ?>

			<h1><?= $model->getTitle(); ?></h1>
		</div>
	</div>
</div>

<div class="page-content page-catalog-home">
    <?php $this->widget('application.modules.store.widgets.CatalogWidget', [
        'view' => 'catalog-home'
    ]); ?>
</div>