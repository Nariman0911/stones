<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-header">
	<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
		'page_id' => $model->id
	]); ?>
	<div class="page-header__heading">
		<div class="content">
			<?php $this->widget('application.components.MyTbBreadcrumbs', [
	            'links' => $this->breadcrumbs,
	        ]); ?>

			<h1><?= $model->getTitle(); ?></h1>
		</div>
	</div>
</div>

<div class="page-content">
    <div class="content">
		<div class="txt-style">
			
			<?= $model->body; ?>

			 <!-- Форма обратной связи -->
			<?php $this->widget('application.modules.mail.widgets.GeneralFeedbackWidget', [
			    'id' => 'contactForm',
			    'formClassName' => 'StandartForm',
			    'buttonModal' => false,
			    'titleModal' => 'Обратная связь',
			    'showCloseButton' => false,
			    'isRefresh' => true,
			    'showAttributeEmail' => true,
			    'showAttributeComment' => true,
			    'eventCode' => 'ostavit-zayavku',
			    'successKey' => 'ostavit-zayavku',
			    'modalHtmlOptions' => [
			        'class' => 'modal-my',
			    ],
			    'formOptions' => [
			        'htmlOptions' => [
			            'class' => 'form-my',
			        ]
			    ],
			    'modelAttributes' => [
			        'theme' => 'Остались вопросы'
			    ],
			    'view' => 'contact-form'
			]) ?>
        </div>
    </div>
</div>
