<?php
/* @var $model Page */
/* @var $this PageController */

if ($model->layout) {
    $this->layout = "//layouts/{$model->layout}";
}

$this->title = $model->meta_title ?: $model->title;
$this->breadcrumbs = $this->getBreadCrumbs();
$this->description = $model->meta_description ?: Yii::app()->getModule('yupe')->siteDescription;
$this->keywords = $model->meta_keywords ?: Yii::app()->getModule('yupe')->siteKeyWords;
?>

<div class="page-header">
	<?php $this->widget('application.modules.slider.widgets.SliderWidget', [
		'page_id' => $model->id
	]); ?>
	<div class="page-header__heading">
		<div class="content">
			<?php $this->widget('application.components.MyTbBreadcrumbs', [
	            'links' => $this->breadcrumbs,
	        ]); ?>

			<h1><?= $model->getTitle(); ?></h1>
		</div>
	</div>
</div>

<div class="page-content">
    <div class="content">
		<div class="txt-style">
			<?= $model->body; ?>
            <?php $photos = $model->photos(['order' => 'photos.position ASC']); ?>
            <?php if($photos) : ?>
                <?php 
                    /*$count = count($photos); 

                    if($count == 1) {
                        $class = 'img-col-12';
                    } elseif(($count % 2 == 0) && $count <= 4) {
                        $class = 'img-col-6';
                    }*/ ?>
                <div class="image-box fl fl-wr-w fl-al-it-c">
                    <?php foreach ($photos as $key => $photo): ?>
                        <div class="image-box__item <?= $class; ?> box-style-img">
                            <a class="fl fl-al-it-c fl-ju-co-c" data-fancybox="image" href="<?= $photo->getImageNewUrl(0, 0, false,null, 'image'); ?>">
                                <picture>
                                    <source media="(min-width: 1px)" srcset="<?= $photo->getImageUrlWebp(212, 125, false,null, 'image'); ?>" type="image/webp">
                                    <source media="(min-width: 1px)" srcset="<?= $photo->getImageNewUrl(212, 125, false,null, 'image'); ?>">

                                    <img src="<?= $photo->getImageNewUrl(212, 125, false,null, 'image'); ?>" alt="<?= $photo->title; ?>">
                                </picture>
                            </a>
                        </div>
                    <?php endforeach ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
