<?php 
    $mainAssets = Yii::app()->getTheme()->getAssetsUrl();
    $hasFlash = Yii::app()->user->hasFlash($this->successKey);
    Yii::app()->getClientScript()->registerScriptFile('https://www.google.com/recaptcha/api.js', CClientScript::POS_END);
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', $this->formOptions) ?>
    <?php if ($hasFlash) : ?>
        <script>
            $("#messageModal").modal('show');
            setTimeout(function(){
                $("#messageModal").modal('hide');
            }, 4000);
        </script>
    <?php endif ?>

    <?= $form->hiddenField($model, 'key', ['value' => $this->id]) ?>

    <div class="row">
        <div class="col-xs-9">
            <div class="row">
                <?php if ($this->showAttributeName) : ?>
                    <div class="col-xs-4">
                        <?= $form->textFieldGroup($model, 'name', [
                            'widgetOptions'=>[
                                'htmlOptions'=>[
                                    'class' => '',
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>
                    </div>
                <?php endif ?>

                <?php if ($this->showAttributePhone) : ?>
                    <div class="col-xs-4">
                        <?= $form->telFieldGroup($model, 'phone', [
                            'widgetOptions' => [
                                'htmlOptions'=>[
                                    'class' => 'phone-mask',
                                    'data-phoneMask' => 'phone',
                                    'placeholder' => Yii::t('MailModule.mail', 'Ваш телефон'),
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>
                    </div>
                <?php endif ?>

                <?php if ($this->showAttributeEmail) : ?>
                    <div class="col-xs-4">
                        <?= $form->textFieldGroup($model, 'email', [
                            'widgetOptions'=>[
                                'htmlOptions'=>[
                                    'class' => '',
                                    'autocomplete' => 'off'
                                ]
                            ]
                        ]); ?>
                    </div>
                <?php endif ?>
            </div>

            <?php if ($this->showAttributeComment) : ?>
                <?= $form->textAreaGroup($model, 'comment') ?>
            <?php endif ?>

            <?= $form->hiddenField($model, 'verifyCode') ?>
            <div class="form-captcha">
                <div class="g-recaptcha" data-sitekey="<?= Yii::app()->params['key']; ?>"></div>
                <?= $form->error($model, 'verify');?>
            </div>
                                    
            <div class="form-bot fl fl-al-it-c">
                <div class="form-button">
                    <button type="submit" class="but but-animate-transform" id="<?= $this->sendButtonId ?>">
                        <span><?= $this->sendButtonText ?></span>
                    </button>
                </div>
                <div class="terms_of_use">
                    Нажимая кнопку Отправить заявку Вы соглашаетесь с <a target="_blank" href="<?php $this->widget('application.modules.contentblock.widgets.ContentMyBlockWidget', ['id' => 4]); ?>">Политикой обработки персональных данных</a>
                </div>
            </div>
        </div>
    </div>
<?php $this->endWidget() ?>

<?php Yii::app()->clientScript->registerScript($this->id.'-script', "
    $(document).delegate('#{$this->formOptions['id']}', 'submit', function() {
        var form = $(this);
        var data = form.serialize();
        var url = form.attr('action');
        var type = form.attr('method');
        var selectorForm = '#{$this->formOptions['id']}';
        $.ajax({
            url: url,
            type: type,
            data: data,
            dataType: 'html',
            success: function(data) {
                $(selectorForm).html($(data).find(selectorForm).html());
                
                if($(selectorForm).find('input[data-mask=phone]').is('.data-mask')){
                    $('[data-mask=phone]').mask(phoneMaskTemplate, {
                        'placeholder':'_',
                        'completed':function() {
                        }
                    });
                }

                if($(selectorForm).find('input[data-phoneMask=phone]').is('.phone-mask')){
                    $('.phone-mask').inputmask(phoneMaskTemplate, {inputmode: 'numeric'});
                }
                
                $.getScript('https://www.google.com/recaptcha/api.js', function () {});
            }
        })

        return false;
    })
") ?>