<?php
Yii::import('bootstrap.components.Booster');

class MyBooster extends Booster {

	protected function registerMetadataForResponsive() {
		if($this->disableZooming)
			$this->cs->registerMetaTag('width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no', 'viewport');
		
	}

}
?>