<?php
/**
 * Файл настроек для модуля document
 *
 * @author yupe team <team@yupe.ru>
 * @link http://yupe.ru
 * @copyright 2009-2017 amyLabs && Yupe! team
 * @package yupe.modules.document.install
 * @since 0.1
 *
 */
return [
    'module'    => [
        'class' => 'application.modules.document.DocumentModule',
    ],
    'import'    => [],
    'component' => [],
    'rules'     => [
        '/document' => 'document/document/index',
    ],
];