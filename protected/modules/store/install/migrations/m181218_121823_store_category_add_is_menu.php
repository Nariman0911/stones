<?php

class m181218_121823_store_category_add_is_menu extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_category}}', 'is_menu', "boolean not null default '0'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_category}}', 'is_menu');
    }
}