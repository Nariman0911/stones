<?php

class m181218_121824_store_add_column_product_doc_vid extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{store_product}}', 'document_id', "integer");
        $this->addColumn('{{store_product}}', 'video_id', "integer");

        $this->createTable(
            '{{store_product_formimage}}',
            [
                'id'       => 'pk',
                'product_id'  => 'integer DEFAULT NULL',
                'image'       => 'string COMMENT "Изображение" not null',
                'title'       => 'string COMMENT "Название изображения" not null',
                'alt'         => 'string COMMENT "Alt изображения" not null',
                'position'    => 'integer COMMENT "Сортировка"',
            ],
            $this->getOptions()
        );

        $this->addForeignKey(
            "fk_{{store_product_formimage}}_product_id",
            '{{store_product_formimage}}',
            'product_id',
            '{{store_product}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('{{store_product}}', 'document_id');
        $this->dropColumn('{{store_product}}', 'video_id');
    }
}