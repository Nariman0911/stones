<?php

Yii::import('application.modules.store.models.*');

class ProductWidget extends yupe\widgets\YWidget
{   
    /**
     * @var
     */
    public $category_id;
    public $view = 'product-widget';

    /**
     * @return bool
     * @throws CException
     */
    public function run()
    {
        $criteria = new CDbCriteria();

        if($this->category_id){
            $criteria->addCondition("t.category_id={$this->category_id}");
        }

        $products = Product::model()->published()->findAll($criteria);

        $this->render(
            $this->view,
            [
                'products' => $products,
            ]
        );
    }
}