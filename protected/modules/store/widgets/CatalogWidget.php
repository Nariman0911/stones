<?php

Yii::import('application.modules.store.models.*');

class CatalogWidget extends yupe\widgets\YWidget
{   
    public $id;
    public $ids;
    public $notIds;
    public $is_home;
    public $is_menu;
    public $category_id = null;
    public $limit;
    public $order = 't.sort ASC';
    
    public $view = 'view';
    protected $models;

    public function run()
    {
        $criteria = new CDbCriteria();

        if($this->limit){
            $criteria->limit = $this->limit;
        }
        
        $criteria->order = $this->order;
        
        $criteria->compare('is_home', $this->is_home);
        $criteria->compare('is_menu', $this->is_menu);
        
        if($this->id){
            $this->models = StoreCategory::model()->published()->findByPk($this->id);
        } else if($this->ids){
            $this->ids = explode(',', $this->ids);
            $this->models = StoreCategory::model()->findAllByPk($this->ids);
        } else if($this->notIds){
            $this->notIds = explode(',', $this->notIds);
            $criteria->addNotInCondition('id', $this->notIds);
            $this->models = StoreCategory::model()->published()->roots()->findAll($criteria);
        } else if($this->category_id){
            $criteria->compare('parent_id', $this->category_id);
            $this->models = StoreCategory::model()->published()->findAll($criteria);
        } else{
            $criteria->addNotInCondition('id', explode(',', Yii::app()->getModule('store')->mostInteresting));
            $this->models = StoreCategory::model()->published()->roots()->findAll($criteria);
        }
        
        $this->render($this->view, [
            'models' => $this->models
        ]);
    }
}