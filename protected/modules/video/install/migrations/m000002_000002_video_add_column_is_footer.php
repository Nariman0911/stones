<?php
/**
 * Video install migration
 * Класс миграций для модуля Video:
 *
 * @category YupeMigration
 * @package  yupe.modules.video.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000002_000002_video_add_column_is_footer extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{video}}', 'is_footer', "boolean not null default '0'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{video}}', 'is_footer');
    }
}
