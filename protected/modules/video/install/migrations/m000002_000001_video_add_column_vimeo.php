<?php
/**
 * Video install migration
 * Класс миграций для модуля Video:
 *
 * @category YupeMigration
 * @package  yupe.modules.video.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000002_000001_video_add_column_vimeo extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{video}}', 'code_vimeo', 'text');
    }

    public function safeDown()
    {
        $this->dropColumn('{{video}}', 'code_vimeo');
    }
}
