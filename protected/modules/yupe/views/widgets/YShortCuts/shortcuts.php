<?php
/**
 * @var $this \yupe\widgets\YShortCuts
 * @var $modules \yupe\components\WebModule[]
 * @var $updates array
 */
?>
<div class="shortcuts">
    <?php foreach ($modules as $module): ?>
        <?php if (!$module->getIsShowInAdminMenu() && !$module->getExtendedNavigation()): ?>
            <?php continue; ?>
        <?php endif; ?>
            <?php $str = ['Другое', 'Юпи!', 'Главная', 'Блоги', 'Категории', 'Комментарии', 'Права доступа', 'Уведомления', 'Новости', 'Изображения', 'Почта', 'Пользователи']; ?>
            <?php if (Yii::app()->getUser()->getProfile()->super_user != 1) {
                if(!array_search($module->getName(), $str)){ ?>
                    <?=  CHtml::link($this->render('_view', ['module' => $module, 'updates' => $updates], true), is_string($module->getAdminPageLink()) ? [$module->getAdminPageLink()] : $module->getAdminPageLink(), ['class' => 'shortcut']); ?>
                <?php } ?>
            <?php } else { ?>
                <?=  CHtml::link($this->render('_view', ['module' => $module, 'updates' => $updates], true), is_string($module->getAdminPageLink()) ? [$module->getAdminPageLink()] : $module->getAdminPageLink(), ['class' => 'shortcut']); ?>
            <?php } ?>
    <?php endforeach; ?>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.config-update').on('click', function (event) {
            var $this = $(this);
            event.preventDefault();
            $.post('<?=  Yii::app()->createUrl('/yupe/modulesBackend/configUpdate/')?>', {
                '<?=  Yii::app()->getRequest()->csrfTokenName;?>': '<?=  Yii::app()->getRequest()->csrfToken;?>',
                'module': $(this).data('module')
            }, function (response) {

                if (response.result) {
                    $this.fadeOut();
                    $('#notifications').notify({
                        message: {text: '<?=  Yii::t('YupeModule.yupe','Successful');?>'},
                        type: 'success'
                    }).show();
                }

            }, 'json');
        });
    });
</script>
