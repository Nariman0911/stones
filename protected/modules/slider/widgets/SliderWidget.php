<?php

Yii::import('application.modules.slider.models.Slider');

class SliderWidget extends yupe\widgets\YWidget
{
    public $page_id;
    public $storecategory_id;
    public $limit = 6;
    /**
     * @var string
     */
    public $view = 'slider-widget';
    protected $models;

    public function init()
    {
        $criteria = new CDbCriteria();
        $criteria->limit = $this->limit;
        $criteria->order = 't.position ASC';

        $criteria->compare("t.page_id", $this->page_id);
        $criteria->compare("t.storecategory_id", $this->storecategory_id);

        $this->models = Slider::model()->findAll($criteria);

        if($this->storecategory_id){
            $this->imageSlide($this->storecategory_id);
        }

        parent::init();
    }

    public function run()
    {
        $this->render($this->view, [
            'models' => $this->models
        ]);
    }

    public function imageSlide($id)
    {
        if(!$this->models){
            $category = StoreCategory::model()->findByPk($id);
            if($category->parent->id){
                $criteria = new CDbCriteria();
                $criteria->limit = $this->limit;
                $criteria->order = 't.position ASC';
                
                $criteria->compare("t.storecategory_id", $category->parent->id);
                $this->models = Slider::model()->findAll($criteria);
                if(!$this->models){
                    $this->imageSlide($category->parent->id);
                }
            }
        }
    }
}
