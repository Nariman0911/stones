<?php

class m000001_000003_slider_add_column_category_id extends yupe\components\DbMigration
{
	public function safeUp()
	{
        $this->addColumn('{{slider}}', 'storecategory_id', 'integer');
	}

	public function safeDown()
	{
        $this->dropColumn('{{slider}}', 'storecategory_id');
	}
}