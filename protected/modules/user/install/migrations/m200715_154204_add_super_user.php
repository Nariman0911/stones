<?php

class m200715_154204_add_super_user extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{user_user}}', 'super_user', 'boolean NOT NULL DEFAULT "0"');

        Yii::app()->getDb()->createCommand('UPDATE {{user_user}} SET super_user=1 WHERE id = 1')->execute();
    }

    public function safeDown()
    {
        $this->dropColumn('{{user_user}}', 'super_user', 'boolean NOT NULL DEFAULT "0"');
    }
}
