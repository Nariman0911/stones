<?php
/**
 * Отображение для update:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('DocumentModule.document', 'Документы') => ['/document/documentBackend/index'],
    $model->name => ['/document/documentBackend/view', 'id' => $model->id],
    Yii::t('DocumentModule.document', 'Редактирование'),
];

$this->pageTitle = Yii::t('DocumentModule.document', 'Документы - редактирование');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DocumentModule.document', 'Управление Документами'), 'url' => ['/document/documentBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DocumentModule.document', 'Добавить Документ'), 'url' => ['/document/documentBackend/create']],
    ['label' => Yii::t('DocumentModule.document', 'Документ') . ' «' . mb_substr($model->id, 0, 32) . '»'],
    ['icon' => 'fa fa-fw fa-pencil', 'label' => Yii::t('DocumentModule.document', 'Редактирование Документа'), 'url' => [
        '/document/documentBackend/update',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-eye', 'label' => Yii::t('DocumentModule.document', 'Просмотреть Документ'), 'url' => [
        '/document/documentBackend/view',
        'id' => $model->id
    ]],
    ['icon' => 'fa fa-fw fa-trash-o', 'label' => Yii::t('DocumentModule.document', 'Удалить Документ'), 'url' => '#', 'linkOptions' => [
        'submit' => ['/document/documentBackend/delete', 'id' => $model->id],
        'confirm' => Yii::t('DocumentModule.document', 'Вы уверены, что хотите удалить Документ?'),
        'csrf' => true,
    ]],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('DocumentModule.document', 'Редактирование') . ' ' . Yii::t('DocumentModule.document', 'Документа'); ?>        <br/>
        <small>&laquo;<?=  $model->name; ?>&raquo;</small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>