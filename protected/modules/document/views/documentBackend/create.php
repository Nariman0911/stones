<?php
/**
 * Отображение для create:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('DocumentModule.document', 'Документы') => ['/document/documentBackend/index'],
    Yii::t('DocumentModule.document', 'Добавление'),
];

$this->pageTitle = Yii::t('DocumentModule.document', 'Документы - добавление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DocumentModule.document', 'Управление Документами'), 'url' => ['/document/documentBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DocumentModule.document', 'Добавить Документ'), 'url' => ['/document/documentBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('DocumentModule.document', 'Документы'); ?>
        <small><?=  Yii::t('DocumentModule.document', 'добавление'); ?></small>
    </h1>
</div>

<?=  $this->renderPartial('_form', ['model' => $model]); ?>