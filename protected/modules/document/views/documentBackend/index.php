<?php
/**
 * Отображение для index:
 *
 *   @category YupeView
 *   @package  yupe
 *   @author   Yupe Team <team@yupe.ru>
 *   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
 *   @link     http://yupe.ru
 **/
$this->breadcrumbs = [
    $this->getModule()->getCategory() => [],
    Yii::t('DocumentModule.document', 'Документы') => ['/document/documentBackend/index'],
    Yii::t('DocumentModule.document', 'Управление'),
];

$this->pageTitle = Yii::t('DocumentModule.document', 'Документы - управление');

$this->menu = [
    ['icon' => 'fa fa-fw fa-list-alt', 'label' => Yii::t('DocumentModule.document', 'Управление Документами'), 'url' => ['/document/documentBackend/index']],
    ['icon' => 'fa fa-fw fa-plus-square', 'label' => Yii::t('DocumentModule.document', 'Добавить Документ'), 'url' => ['/document/documentBackend/create']],
];
?>
<div class="page-header">
    <h1>
        <?=  Yii::t('DocumentModule.document', 'Документы'); ?>
        <small><?=  Yii::t('DocumentModule.document', 'управление'); ?></small>
    </h1>
</div>

<p>
    <a class="btn btn-default btn-sm dropdown-toggle" data-toggle="collapse" data-target="#search-toggle">
        <i class="fa fa-search">&nbsp;</i>
        <?=  Yii::t('DocumentModule.document', 'Поиск Документов');?>
        <span class="caret">&nbsp;</span>
    </a>
</p>

<div id="search-toggle" class="collapse out search-form">
        <?php Yii::app()->clientScript->registerScript('search', "
        $('.search-form form').submit(function () {
            $.fn.yiiGridView.update('document-grid', {
                data: $(this).serialize()
            });

            return false;
        });
    ");
    $this->renderPartial('_search', ['model' => $model]);
?>
</div>

<br/>

<p> <?=  Yii::t('DocumentModule.document', 'В данном разделе представлены средства управления Документами'); ?>
</p>

<?php
 $this->widget(
    'yupe\widgets\CustomGridView',
    [
        'id'           => 'document-grid',
        'sortableRows'      => true,
        'sortableAjaxSave'  => true,
        'sortableAttribute' => 'position',
        'sortableAction'    => '/document/documentBackend/sortable',
        'type'         => 'striped condensed',
        'dataProvider' => $model->search(),
        'filter'       => $model,
        'columns'      => [
            [
                'type' => 'raw',
                'value' => function ($data) {
                    return CHtml::image($data->getImageUrl(100, 140, true), '', ["width" => 50, "height" => 70, "class" => "img-thumbnail"]);
                },
            ],
            [
                'class'    => 'bootstrap.widgets.TbEditableColumn',
                'name'     => 'name',
                'editable' => [
                    'url'    => $this->createUrl('/document/documentBackend/inline'),
                    'mode'   => 'inline',
                    'params' => [
                        Yii::app()->request->csrfTokenName => Yii::app()->request->csrfToken
                    ]
                ],
                'filter'   => CHtml::activeTextField($model, 'name', ['class' => 'form-control']),
            ],
            'file_name',
            [
                'class' => 'yupe\widgets\EditableStatusColumn',
                'name' => 'status',
                'url' => $this->createUrl('/document/documentBackend/inline'),
                'source' => $model->getStatusList(),
                'options' => [
                    Document::STATUS_PUBLIC => ['class' => 'label-success'],
                    Document::STATUS_MODERATE => ['class' => 'label-default'],
                ],
            ],
            [
                'class' => 'yupe\widgets\CustomButtonColumn',
            ],
        ],
    ]
); ?>
