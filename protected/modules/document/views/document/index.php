<?php
/**
* Отображение для document/index
*
* @category YupeView
* @package  yupe
* @author   Yupe Team <team@yupe.ru>
* @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
* @link     http://yupe.ru
**/
$this->pageTitle = Yii::t('DocumentModule.document', 'document');
$this->description = Yii::t('DocumentModule.document', 'document');
$this->keywords = Yii::t('DocumentModule.document', 'document');

$this->breadcrumbs = [Yii::t('DocumentModule.document', 'document')];
?>

<h1>
    <small>
        <?php echo Yii::t('DocumentModule.document', 'document'); ?>
    </small>
</h1>