<?php

/**
 * NewGalleryWidget виджет отрисовки галереи изображений
 *
 */

Yii::import('application.modules.document.models.*');

class DocumentWidget extends yupe\widgets\YWidget
{
    public $view = 'view';

    public function run()
    {
        $criteria = new CDbCriteria();

        $criteria->addCondition("product_id is null");
        $criteria->order = 't.position ASC';

        $model = Document::model()->published()->findAll($criteria);

        $this->render($this->view, [
            'model' => $model,
        ]);
    }
}
