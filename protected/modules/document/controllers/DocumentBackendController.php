<?php
/**
* Класс DocumentBackendController:
*
*   @category Yupe\yupe\components\controllers\BackController
*   @package  yupe
*   @author   Yupe Team <team@yupe.ru>
*   @license  https://github.com/yupe/yupe/blob/master/LICENSE BSD
*   @link     http://yupe.ru
**/
class DocumentBackendController extends \yupe\components\controllers\BackController
{
    public function actions()
    {
        return [
            'inline' => [
                'class'           => 'yupe\components\actions\YInLineEditAction',
                'model'           => 'Document',
                'validAttributes' => [
                    'status', 'name',
                ]
            ],
            'sortable' => [
                'class' => 'yupe\components\actions\SortAction',
                'model' => 'Document',
            ],
        ];
    }
    /**
    * Отображает Документ по указанному идентификатору
    *
    * @param integer $id Идинтификатор Документ для отображения
    *
    * @return void
    */
    public function actionView($id)
    {
        $this->render('view', ['model' => $this->loadModel($id)]);
    }
    
    /**
    * Создает новую модель Документа.
    * Если создание прошло успешно - перенаправляет на просмотр.
    *
    * @return void
    */
    public function actionCreate()
    {
        $model = new Document;

        if (Yii::app()->getRequest()->getPost('Document') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Document'));
        
            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DocumentModule.document', 'Запись добавлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('create', ['model' => $model]);
    }
    
    /**
    * Редактирование Документа.
    *
    * @param integer $id Идинтификатор Документ для редактирования
    *
    * @return void
    */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        if (Yii::app()->getRequest()->getPost('Document') !== null) {
            $model->setAttributes(Yii::app()->getRequest()->getPost('Document'));

            if($model->image){
                if($_FILES['Document']['name']['image'] || $_POST['delete-file1']){
                    $temp = Yii::getPathOfAlias('webroot').'/uploads/document/image/'.$model->image;
                    unlink($temp);
                    if($_POST['delete-file1']){
                        $model->image = '';
                    }
                }
            }
            if($model->file_name){
                if($_FILES['Document']['name']['file_name'] || $_POST['delete-file3']){
                    $temp = Yii::getPathOfAlias('webroot').'/uploads/document/file/'.$model->file_name;
                    unlink($temp);
                    if($_POST['delete-file3']){
                        $model->file_name = '';
                    }
                }
            }

            if ($model->save()) {
                Yii::app()->user->setFlash(
                    yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                    Yii::t('DocumentModule.document', 'Запись обновлена!')
                );

                $this->redirect(
                    (array)Yii::app()->getRequest()->getPost(
                        'submit-type',
                        [
                            'update',
                            'id' => $model->id
                        ]
                    )
                );
            }
        }
        $this->render('update', ['model' => $model]);
    }
    
    /**
    * Удаляет модель Документа из базы.
    * Если удаление прошло успешно - возвращется в index
    *
    * @param integer $id идентификатор Документа, который нужно удалить
    *
    * @return void
    */
    public function actionDelete($id)
    {
        if (Yii::app()->getRequest()->getIsPostRequest()) {
            // поддерживаем удаление только из POST-запроса
            $this->loadModel($id)->delete();

            Yii::app()->user->setFlash(
                yupe\widgets\YFlashMessages::SUCCESS_MESSAGE,
                Yii::t('DocumentModule.document', 'Запись удалена!')
            );

            // если это AJAX запрос ( кликнули удаление в админском grid view), мы не должны никуда редиректить
            if (!Yii::app()->getRequest()->getIsAjaxRequest()) {
                $this->redirect(Yii::app()->getRequest()->getPost('returnUrl', ['index']));
            }
        } else
            throw new CHttpException(400, Yii::t('DocumentModule.document', 'Неверный запрос. Пожалуйста, больше не повторяйте такие запросы'));
    }
    
    /**
    * Управление Документами.
    *
    * @return void
    */
    public function actionIndex()
    {
        $model = new Document('search');
        $model->unsetAttributes(); // clear any default values
        if (Yii::app()->getRequest()->getParam('Document') !== null)
            $model->setAttributes(Yii::app()->getRequest()->getParam('Document'));
        $this->render('index', ['model' => $model]);
    }
    
    /**
    * Возвращает модель по указанному идентификатору
    * Если модель не будет найдена - возникнет HTTP-исключение.
    *
    * @param integer идентификатор нужной модели
    *
    * @return void
    */
    public function loadModel($id)
    {
        $model = Document::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, Yii::t('DocumentModule.document', 'Запрошенная страница не найдена.'));

        return $model;
    }
}
