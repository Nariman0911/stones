<?php
/**
* DocumentController контроллер для document на публичной части сайта
*
* @author yupe team <team@yupe.ru>
* @link http://yupe.ru
* @copyright 2009-2017 amyLabs && Yupe! team
* @package yupe.modules.document.controllers
* @since 0.1
*
*/

class DocumentController extends \yupe\components\controllers\FrontController
{
    /**
     * Действие "по умолчанию"
     *
     * @return void
     */
    public function actionIndex()
    {
        $this->render('index');
    }
}