<?php
/**
 * Document install migration
 * Класс миграций для модуля Document:
 *
 * @category YupeMigration
 * @package  yupe.modules.document.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000000_document_base extends yupe\components\DbMigration
{
    /**
     * Функция настройки и создания таблицы:
     *
     * @return null
     **/
    public function safeUp()
    {
        $this->createTable(
            '{{document}}',
            [
                'id'        => 'pk',
                'name'      => 'string COMMENT "Название документа"',
                'file_name' => 'string COMMENT "Имя файла"',
                'status'    => 'integer default "0" COMMENT "Статус"',
                'position'  => 'integer COMMENT "Сортировка"',
            ],
            $this->getOptions()
        );
    }

    /**
     * Функция удаления таблицы:
     *
     * @return null
     **/
    public function safeDown()
    {
        $this->dropTableWithForeignKeys('{{document}}');
    }
}
