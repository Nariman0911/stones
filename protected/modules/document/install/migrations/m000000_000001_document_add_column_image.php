<?php
/**
 * Generators install migration
 * Класс миграций для модуля Generators:
 *
 * @category YupeMigration
 * @package  yupe.modules.generators.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000001_document_add_column_image extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{document}}', 'image', 'string COMMENT "Превью (изображение)"');
    }

    public function safeDown()
    {
        $this->dropColumn('{{document}}', 'image');
    }
}