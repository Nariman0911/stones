<?php
/**
 * Generators install migration
 * Класс миграций для модуля Generators:
 *
 * @category YupeMigration
 * @package  yupe.modules.generators.install.migrations
 * @author   YupeTeam <team@yupe.ru>
 * @license  BSD https://raw.github.com/yupe/yupe/master/LICENSE
 * @link     http://yupe.ru
 **/
class m000000_000002_document_add_column_product extends yupe\components\DbMigration
{
    public function safeUp()
    {
        $this->addColumn('{{document}}', 'product_id', 'integer COMMENT "Для Продукта"');
    }

    public function safeDown()
    {
        $this->dropColumn('{{document}}', 'product_id');
    }
}