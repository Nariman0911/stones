<?php

/**
 * This is the model class for table "{{document}}".
 *
 * The followings are the available columns in table '{{document}}':
 * @property integer $id
 * @property string $name
 * @property string $file_name
 * @property integer $status
 * @property integer $position
 * @property integer $product_id
 */
class Document extends yupe\models\YModel
{
	const STATUS_PUBLIC = 1;
	const STATUS_MODERATE = 0;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{document}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status, position, product_id', 'numerical', 'integerOnly'=>true),
			array('name, file_name, image', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, file_name, image, status, position, product_id', 'safe', 'on'=>'search'),
			array('product_id', 'safe'),
		);
	}
	public function behaviors()
    {
        $module = Yii::app()->getModule('document');

        return [
        	'upload' => [
                'class' => 'yupe\components\behaviors\ImageUploadBehavior',
                'attributeName'  => 'image',
                'minSize'        => 0,
                'maxSize'        => $module->maxSize,
                'types'          => 'jpg, jpeg, png',
                'uploadPath'     => $module->uploadPath.'/image',
                'resizeOnUpload' => true,
                'resizeOptions' => [
                    'maxWidth' => 500,
                    'maxHeight' => 500,
                ],
            ],
        	'fileUpload' => [
                'class'         => 'yupe\components\behaviors\FileUploadBehavior',
                'attributeName' => 'file_name',
                'maxSize'       => $module->maxSize,
                'types'         => 'pdf, doc, docx',
                'uploadPath'    => $module->uploadPath.'/file',
            ],
        	'sortable' => [
                'class' => 'yupe\components\behaviors\SortableBehavior',
            ],
        ];
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return [
		];
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Название документа',
			'file_name' => 'Файл (doc, docx, pdf)',
			'image' => 'Превью (изображение 250x350)',
			'status' => 'Статус',
			'position' => 'Сортировка',
			'product_id' => 'Документ для продукта',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('position',$this->position);
		$criteria->compare('product_id',$this->product_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => ['defaultOrder' => 't.position'],
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Document the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getStatusList()
	{
		return [
			self::STATUS_PUBLIC   => 'Опубликован',
			self::STATUS_MODERATE => 'На модерации',
		];
	}

	public function getStatusName()
	{
		$data = $this->getStatusList();
		if (isset($data[$this->status])) {
			return $data[$this->status];
		}
		return null;
	}

	public function scopes()
    {
        return [
            'published' => [
                'condition' => 'status  = :status',
                'params' => [
                    ':status' => self::STATUS_PUBLIC
                ]
            ],
        ];
    }

    public function getDocument(){
    	if(isset($this->file_name)) {
	    	$module = Yii::app()->getModule('document');
	    	return '/uploads/document/file/'.$this->file_name;
    	} else{
	    	return '#';
    	}
    }
}
